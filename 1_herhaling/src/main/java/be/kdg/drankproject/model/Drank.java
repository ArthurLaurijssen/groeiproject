package be.kdg.drankproject.model;

import java.time.LocalDate;
import java.util.List;

public class Drank implements Comparable<Drank>{
    private String naam,herkomst,barcode;
    private Categorie categorie;
    private LocalDate uitvindingDatum;
    private double inhoud;
    private int verpaktPer;
    private static int aantal = 0;

    public Drank() {
        setNaam("ongekent");
        setHerkomst("anoniem");
        setCategorie(Categorie.ONGEKENT);
        setUitvindingDatum(LocalDate.of(LocalDate.now().getYear(),LocalDate.now().getMonth(),LocalDate.now().getDayOfMonth()-1));
        setInhoud(1.0);
        setVerpaktPer(1);
        setBarcode("012345678910");
        aantal++;
    }
    public Drank(String naam, String herkomst, Categorie categorie, LocalDate uitvindingDatum, double inhoud, int verpaktPer,String barcode) {
        setNaam(naam);
        setHerkomst(herkomst);
        setCategorie(categorie);
        setBarcode(barcode);
        setUitvindingDatum(uitvindingDatum);
        setInhoud(inhoud);
        setVerpaktPer(verpaktPer);
        aantal++;
    }

    public void setNaam(String naam) {
        try {
            if (naam.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.naam = naam;
        } catch(IllegalArgumentException e) {
            System.out.println("Naam mag niet leeg zijn");
        }
    }

    public void setHerkomst(String herkomst) {
        try {
            if (herkomst.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.herkomst = herkomst;
        } catch(IllegalArgumentException e) {
            System.out.println("Herkomst kan niet leeg zijn");
        }
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public void setUitvindingDatum(LocalDate uitvindingDatum) {
        try {
            if (!uitvindingDatum.isBefore(LocalDate.now()))  {
                throw new IllegalArgumentException();
            }
            this.uitvindingDatum= uitvindingDatum;
        } catch(IllegalArgumentException e) {
            System.out.println("Kan niet in de toekomst zijn");
        }
    }

    public void setInhoud(double inhoud) {
        try {
            if (inhoud<=0) {
                throw new IllegalArgumentException();
            }
            this.inhoud = inhoud;
        } catch(IllegalArgumentException e) {
            System.out.println("Inhoud kan niet 0 of onder de 0 liggen.");
        }
    }

    public void setVerpaktPer(int verpaktPer) {
        try {
            if (verpaktPer<=0) {
                throw new IllegalArgumentException();
            }
            this.inhoud = inhoud;
        } catch(IllegalArgumentException e) {
            System.out.println("Verpakt per kan niet 0 of onder de 0 liggen.");
        }
    }

    public void setBarcode(String barcode) {
        try {
            if (barcode.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.barcode = barcode;
        } catch(IllegalArgumentException e) {
            System.out.println("Barcode kan niet leeg zijn!");
        }
    }

    public String getNaam() {
        return naam;
    }

    public String getHerkomst() {
        return herkomst;
    }

    public String getBarcode() {
        return barcode;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public LocalDate getUitvindingDatum() {
        return uitvindingDatum;
    }

    public double getInhoud() {
        return inhoud;
    }

    public int getVerpaktPer() {
        return verpaktPer;
    }

    public static int getAantal() {
        return aantal;
    }

    @Override
    public int hashCode() {
        return this.barcode.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Drank object = (Drank) obj;
        if (object.barcode.equals(this.barcode)) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Drank drank) {
        return this.getBarcode().compareTo(drank.getBarcode());
    }
    @Override
    public String toString() {
        return String.format("%-15s (%s) \t %-11s  %s",this.naam,this.uitvindingDatum.getYear(),this.herkomst,this.categorie.toString());
    }

}
