package be.kdg;

import be.kdg.drankproject.data.Data;
import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;

import java.time.LocalDate;

public class Demo_1 {
    public static void main(String[] args) {
        Dranken dranken = new Dranken();

        for (Drank drank: Data.getData()) {
            dranken.voegToe(drank);
        }
        System.out.println("Dranken voegtoe: " + dranken.voegToe(new Drank("Coca Cola" , "USA", Categorie.FRISDRANK, LocalDate.of(1950,10,25),33.0,6,"12445678610")));
        System.out.println(dranken.zoek("Coca Cola").toString());
        System.out.println("Aantal "+ dranken.getAantal());
        System.out.println("Verwijder: " +dranken.verwijder("Coca Cola"));
        System.out.println("\n\nDranken gesorteerd op naam:");

        for (Drank drank: dranken.gesorteerdOpNaam()) {
            System.out.println(drank.toString());
        }
        System.out.println("\n\nDranken gesorteerd op uitvindingsdatum:");
        for (Drank drank: dranken.gesorteerdOpDatum()) {
            System.out.println(drank.toString());
        }
        System.out.println("\n\nDranken gesorteerd op herkomst:");
        for (Drank drank: dranken.gesorteerdOpHerkomst()) {
            System.out.println(drank.toString());
        }
    }
}
