package be.kdg.drankproject.persist;

import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.model.Drank;
import org.hsqldb.SqlInvariants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class DrankDbDao implements DrankDao {

    private static DrankDbDao enigeDrankDBDao;

    private Connection connection;
    private String databasePath;

    public DrankDbDao(String databasePath) {
        this.databasePath = databasePath;
        createConnection();
        createTable();
    }

    private void createConnection() {
        try {
            this.connection = DriverManager.getConnection("jdbc:hsqldb:file:" + this.databasePath, "sa", "");

        }catch (SQLException e) {
            System.out.println("kan geen verbinding maken met de database");
        }
    }

    private void createTable() {
        try {
            Statement connectionStatement = connection.createStatement();
            connectionStatement.execute("DROP TABLE dranktable IF EXISTS");
            String createQuery = "CREATE TABLE dranktable (id INTEGER IDENTITY,naam VARCHAR(30),herkomst VARCHAR(30),categorie VARCHAR(15),uitvindingsdatum DATE,inhoud DOUBLE,verpaktper INTEGER,barcode VARCHAR(20))";
            connectionStatement.execute(createQuery);
        } catch (SQLException e) {
            System.err.println("Onverwachte fout bij aanmaken tabel: " + e.getMessage());
        }
    }

    public void close() {
        if (this.connection == null) {
            return;
        }
        try {
            Statement connectionStatement = connection.createStatement();
            connectionStatement.execute("SHUTDOWN COMPACT;");
            connectionStatement.close();
            connection.close();
            System.out.println("\nDatabase gesloten");
        } catch (SQLException e) {
            System.out.println("Probleem bij sluiten van database: " + e.getMessage());
        }
    }


    @Override
    public boolean insert(Drank drank) {
        if (drank.getId() >= 0)  {
            return false; //drank heeft al PK dus bestaat al in database
        }
      try {
          PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO dranktable(naam,herkomst,categorie,uitvindingsdatum,inhoud,verpaktper,barcode) VALUES(?,?,?,?,?,?,?)");
          preparedStatement.setString(1,drank.getNaam());
          preparedStatement.setString(2,drank.getHerkomst());
          preparedStatement.setString(3,drank.getCategorie().name());
          preparedStatement.setDate(4,Date.valueOf(drank.getUitvindingDatum()));
          preparedStatement.setDouble(5,drank.getInhoud());
          preparedStatement.setInt(6,drank.getVerpaktPer());
          preparedStatement.setString(7,drank.getBarcode());
          preparedStatement.executeUpdate();
          preparedStatement.close();

            return true;
          /*
          Statement statement = connection.createStatement();
          ResultSet rs = statement.executeQuery("SELECT id FROM dranktable WHERE naam='" + drank.getNaam() + "'");
          drank.setId(rs.getInt("id"));
          */

      } catch (SQLException e) {
          e.printStackTrace();
      }
        return false;
    }

    @Override
    public boolean delete(String naam) {
        /*
         delete: Als parameter komt een String waarop gezocht moet worden. Verwijder het record dat hieraan voldoet.
Als er “*” meegegeven wordt als parameter, dan verwijder je ALLE records.
         */
        if (retrieve(naam)==null) {
            return false;
        }
        if (naam.equals('*')) {
            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate("DELETE FROM dranktable");
                return true;
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate("DELETE FROM dranktable WHERE naam='" + naam + "'");
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean update(Drank drank) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE dranktable SET naam=?,herkomst=?,categorie=?,uitvindingsdatum=?,inhoud=?,verpaktper=?,barcode=? WHERE id = ?");
            preparedStatement.setString(1,drank.getNaam());
            preparedStatement.setString(2,drank.getHerkomst());
            preparedStatement.setString(3,drank.getCategorie().name());
            preparedStatement.setDate(4,Date.valueOf(drank.getUitvindingDatum()));
            preparedStatement.setDouble(5,drank.getInhoud());
            preparedStatement.setInt(6,drank.getVerpaktPer());
            preparedStatement.setString(7,drank.getBarcode());
            preparedStatement.setInt(8,drank.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Drank retrieve(String naam) {
        ResultSet rs = null;
        try {
            Statement statement = this.connection.createStatement();
            rs = statement.executeQuery("SELECT * FROM dranktable WHERE naam='" + naam + "'");
            while (rs.next()) {
                Drank drank = new Drank(rs.getInt("id"),rs.getString("naam"),rs.getString("herkomst"), Categorie.valueOf(rs.getString("categorie")),rs.getDate("uitvindingsdatum").toLocalDate(),rs.getDouble("inhoud"),rs.getInt("verpaktper"),rs.getString("barcode"));
                return drank;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<Drank> sortedOn(String query) {
        /*
        sortedOn: Als parameter komt een query-string binnen.
        Laat het sorteren over aan de databank (ORDER BY).
         Als resultaat geef je een gesorteerde List terug.
         */
        ResultSet rs = null;
        List<Drank> myList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            rs = statement.executeQuery(query);
            while (rs.next()) {
                myList.add( new Drank(rs.getInt("id"),rs.getString("naam"),rs.getString("herkomst"), Categorie.valueOf(rs.getString("categorie")),rs.getDate("uitvindingsdatum").toLocalDate(),rs.getDouble("inhoud"),rs.getInt("verpaktper"),rs.getString("barcode")));
            }
        } catch (SQLException e) {
            System.err.println("Fout bij create: " + e);
        }
        return myList;
    }
    public static synchronized DrankDbDao getInstance(String path) {
        if (enigeDrankDBDao == null) {
            enigeDrankDBDao = new DrankDbDao(path);
        }
        return enigeDrankDBDao;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return enigeDrankDBDao;
    }
}
