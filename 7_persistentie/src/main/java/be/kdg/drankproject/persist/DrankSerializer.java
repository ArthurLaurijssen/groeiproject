package be.kdg.drankproject.persist;

import be.kdg.drankproject.model.Dranken;

import java.io.*;

public class DrankSerializer {
    private static final String FILENAME = "db/dictators.ser";

    public void serialize(Dranken dranken) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(FILENAME));
        objectOutputStream.writeObject(dranken);
    }

    public Dranken deserialize() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(FILENAME));
        Dranken dranken = (Dranken) objectInputStream.readObject();
        return dranken;
    }
}
