package be.kdg.drankproject.persist;

import be.kdg.drankproject.model.Drank;

import java.util.List;

public interface DrankDao {
    boolean insert(Drank drank);
    boolean delete(String naam);
    boolean update(Drank drank);
    Drank retrieve(String naam);
    List<Drank> sortedOn(String query);
}
