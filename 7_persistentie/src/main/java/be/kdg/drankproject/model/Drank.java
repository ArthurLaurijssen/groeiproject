package be.kdg.drankproject.model;

import java.io.Serializable;
import java.time.LocalDate;

public class Drank implements Comparable<Drank>, Serializable {
    private int id; //pk

    private String naam,herkomst,barcode;
    private Categorie categorie;
    private LocalDate uitvindingDatum;
    private transient double inhoud;
    private transient int verpaktPer;
    private static int aantal = 0;
    private static final long serialVersionUID = 1L;

    public Drank() {
        this.naam = "ongekent";
        this.herkomst = "anoniem";
        this.categorie = Categorie.ONGEKENT;
        this.uitvindingDatum = LocalDate.of(LocalDate.now().getYear(),LocalDate.now().getMonth(),LocalDate.now().getDayOfMonth()-1);
        this.inhoud = 0.0;
        this.verpaktPer = 0;
        this.barcode = "012345678910";
        aantal++;
    }
    public Drank(String naam, String herkomst, Categorie categorie, LocalDate uitvindingDatum, double inhoud, int verpaktPer,String barcode) {
        this.setId(-1);
        this.setNaam(naam);
        this.setHerkomst(herkomst);
        this.setCategorie(categorie);
        this.setUitvindingDatum(uitvindingDatum);
        this.setInhoud(inhoud);
        this.setVerpaktPer(verpaktPer);
        this.setBarcode(barcode);
        aantal++;
    }
    public Drank(int id,String naam, String herkomst, Categorie categorie, LocalDate uitvindingDatum, double inhoud, int verpaktPer,String barcode) {
        this.setId(id);
        this.setNaam(naam);
        this.setHerkomst(herkomst);
        this.setCategorie(categorie);
        this.setUitvindingDatum(uitvindingDatum);
        this.setInhoud(inhoud);
        this.setVerpaktPer(verpaktPer);
        this.setBarcode(barcode);
        aantal++;
    }

    public void setNaam(String naam) {
        try {
            if (naam.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.naam = naam;
        } catch(IllegalArgumentException e) {
            System.out.println("Naam mag niet leeg zijn");
        }
    }

    public void setHerkomst(String herkomst) {
        try {
            if (herkomst.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.herkomst = herkomst;
        } catch(IllegalArgumentException e) {
            System.out.println("Herkomst kan niet leeg zijn");
        }
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public void setUitvindingDatum(LocalDate uitvindingDatum) {
        try {
            if (!uitvindingDatum.isBefore(LocalDate.now()))  {
                throw new IllegalArgumentException();
            }
            this.uitvindingDatum= uitvindingDatum;
        } catch(IllegalArgumentException e) {
            System.out.println("Kan niet in de toekomst zijn");
        }
    }

    public void setInhoud(double inhoud) {
        try {
            if (inhoud<=0) {
                throw new IllegalArgumentException();
            }
            this.inhoud = inhoud;
        } catch(IllegalArgumentException e) {
            System.out.println("Inhoud kan niet 0 of onder de 0 liggen.");
        }
    }

    public void setVerpaktPer(int verpaktPer) {
        try {
            if (verpaktPer<=0) {
                throw new IllegalArgumentException();
            }
            this.inhoud = inhoud;
        } catch(IllegalArgumentException e) {
            System.out.println("Verpakt per kan niet 0 of onder de 0 liggen.");
        }
    }

    public void setBarcode(String barcode) {
        try {
            if (barcode.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.barcode = barcode;
        } catch(IllegalArgumentException e) {
            System.out.println("Barcode kan niet leeg zijn!");
        }
    }

    public String getNaam() {
        return naam;
    }

    public String getHerkomst() {
        return herkomst;
    }

    public String getBarcode() {
        return barcode;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public LocalDate getUitvindingDatum() {
        return uitvindingDatum;
    }

    public double getInhoud() {
        return inhoud;
    }

    public int getVerpaktPer() {
        return verpaktPer;
    }

    public static int getAantal() {
        return aantal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return this.barcode.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Drank object = (Drank) obj;
        if (object.barcode.equals(this.barcode)) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Drank drank) {
        return this.getBarcode().compareTo(drank.getBarcode());
    }
    @Override
    public String toString() {
        return String.format("%-15s (%s) \t %-11s  %s",this.naam,this.uitvindingDatum.getYear(),this.herkomst,this.categorie.toString());
    }

}
