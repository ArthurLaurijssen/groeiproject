package be.kdg.drankproject.persist;



import be.kdg.drankproject.data.Data;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;
import org.junit.*;

import javax.swing.*;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class DrankDbDaoTest {

    /*
    4.2. Pas de testklasse XxxDbDaoTest aan:
a. In de @BeforeClass methode vraag je de enige instantie op.
b. Schrijf een nieuwe testmethode testSingleton waarin je een tweede instantie opvraagt en
controleert of beide objecten hetzelfde zijn (assertSame) Probeer ook de clone-methode uit.

     */

    private static DrankDbDao drankDbDao;


    @BeforeClass
    public static void setUpClass() throws Exception {
        drankDbDao = DrankDbDao.getInstance("7_persistentie/db/drankdb");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        drankDbDao.close();
    }

    @Before
    public void setUp() throws Exception {
        for (Drank drank : Data.getData()) {
            drankDbDao.insert(drank);
        }
    }

    @After
    public void tearDown() throws Exception {
        for (Drank drank : Data.getData()) {
            drankDbDao.delete(drank.getNaam());
        }
    }

    @Test
    public void testInsert() {
        assertEquals("Zou gelijk moeten zijn",drankDbDao.sortedOn("SELECT * FROM dranktable ORDER BY naam").size(),Data.getData().size());
    }

    @Test
    public void testRetrieveUpdate() {
        Drank drank = drankDbDao.retrieve("Fanta");
        drank.setNaam("Fanta2");
        assertTrue("Zou true moeten geven",drankDbDao.update(drank));
        drank = drankDbDao.retrieve("Fanta2");
        drank.setNaam("Fanta");
        assertTrue("Zou true moeten geven",drankDbDao.update(drank));
    }

    @Test
    public void testDelete() {
        assertTrue("Zou true moeten geven om de eerste keer een object te verwijderen",drankDbDao.delete(Data.getData().get(1).getNaam()));
        assertFalse("Zou false moeten geven na een 2de keer hetzelfde object te verwijderen",drankDbDao.delete(Data.getData().get(1).getNaam()));
        assertTrue("Het terugvoegen zou moeten lukken",drankDbDao.insert(Data.getData().get(1)));
    }
    /*
    testSort: Vergelijk een gesorteerde List uit de Data-klasse met het resultaat van de
sortedOnXxx methode van je Dao-klasse.
     */

    @Test
    public void testSort() {
        Dranken dranken = new Dranken();
        for (Drank drank:Data.getData()) {
            dranken.voegToe(drank);
        }
        List<Drank> drankList = drankDbDao.sortedOn("SELECT * FROM dranktable ORDER BY naam");
        List<Drank> drankList1 = dranken.gesorteerdOpNaam();
        for (int i = 0;i<drankList1.size();i++) {
            assertEquals("Zou gelijk gesorteerd moeten zijn",drankList.get(i).getNaam(),drankList1.get(i).getNaam());
        }
    }

    @Test
    public void testSingleton() {
        assertEquals("Zouden gelijk moeten zijn aangezien het dezelfde instantie is",DrankDbDao.getInstance("7_persistentie/db/drankdb"),drankDbDao);
    }

    @Test
    public void testCloen() {
        try {
            assertEquals("Zouden gelijk moeten zijn aangezien het dezelfde instantie is", drankDbDao.clone(), drankDbDao);
        } catch (CloneNotSupportedException e) {
            fail("Zou geen fout mogen geven");
        }
    }
}
