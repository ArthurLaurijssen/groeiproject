package be.kdg.drankproject.persist;


import be.kdg.drankproject.data.Data;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class DrankSerializerTest {

    private Dranken dranken;


    @Before
    public void setUp() throws Exception {
        dranken = new Dranken();
        for (Drank drank : Data.getData()) {
            dranken.voegToe(drank);
        }
    }

    @Test
    public void testSerialize() {
        try {
            DrankSerializer drankSerializer = new DrankSerializer();
            drankSerializer.serialize(dranken);
        } catch (IOException e) {
            fail("Zou geen geen foutmelding mogen geven");
        }
    }

    @Test
    public void testDeserialize() {
         /*
c. Schrijf een testmethode testDeserialize die controleert of het gedesialiseerd object dezelfde
inhoud bevat als het oorspronkelijk multiobject en er geen exception optreedt.
     */
        Dranken dranken1 = new Dranken();
        try {
            DrankSerializer drankSerializer = new DrankSerializer();
            dranken1 = drankSerializer.deserialize();
        } catch (IOException | ClassNotFoundException e) {
            fail("Zou geen geen foutmelding mogen geven");
        }
        List<Drank> drankList = dranken.gesorteerdOpDatum();
        List<Drank> drankList2 = dranken1.gesorteerdOpDatum();
        for (int i = 0;i<drankList.size();i++) {
            assertEquals("Zou dezelfde naam moeten zijn",drankList.get(i).getNaam(),drankList2.get(i).getNaam());
        }
    }
}
