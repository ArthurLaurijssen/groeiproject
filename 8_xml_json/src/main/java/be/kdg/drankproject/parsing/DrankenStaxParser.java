package be.kdg.drankproject.parsing;

import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DrankenStaxParser {
    private Dranken dranken;
    private String fileName;

    private XMLStreamWriter xmlStreamWriter;

    public DrankenStaxParser(Dranken dranken, String fileName) {
        this.dranken = dranken;
        this.fileName = fileName;

        try {
            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(new FileWriter(fileName));
            xmlStreamWriter = new IndentingXMLStreamWriter(xmlStreamWriter);
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public void WriteXML() throws XMLStreamException,IOException {
        xmlStreamWriter.writeStartDocument();
        xmlStreamWriter.writeStartElement("dranken");
        for (Drank drank: this.dranken.gesorteerdOpNaam()) {
            writeElement(drank);
        }
        xmlStreamWriter.writeEndElement();
        xmlStreamWriter.writeEndDocument();
        xmlStreamWriter.close();
    }

    private void writeElement(Drank drank) throws XMLStreamException {
        xmlStreamWriter.writeStartElement("drank");
        xmlStreamWriter.writeAttribute("categorie",drank.getCategorie().toString());
        writeSimpleElement("naam",drank.getNaam());
        writeSimpleElement("herkomst",drank.getHerkomst());
        writeSimpleElement("barcode",drank.getBarcode());
        writeSimpleElement("uitvindingsdatum",drank.getUitvindingDatum().toString());
        writeSimpleElement("inhoud",String.valueOf(drank.getInhoud()));
        writeSimpleElement("verpaktper",String.valueOf(drank.getVerpaktPer()));
        xmlStreamWriter.writeEndElement();
    }

    private void writeSimpleElement(String naam, String inhoud) throws XMLStreamException {
        xmlStreamWriter.writeStartElement(naam);
        xmlStreamWriter.writeCharacters(inhoud);
        xmlStreamWriter.writeEndElement();
    }





    /*public static void StaxWriteXML(Map<String, List<Aap>> myMap, String fileName) throws XMLStreamException, IOException {


		//Specifieke xmlStreamWriter om indenting in de XML-output in te voegen:
		xmlStreamWriter.writeStartDocument();


		xmlStreamWriter.writeStartElement("apen");
		for (String s : myMap.keySet()) {
        xmlStreamWriter.writeStartElement("soort");
        for (Aap aap : myMap.get(s)) {
            xmlStreamWriter.writeStartElement("aap");
            writeSimpleElement(xmlStreamWriter, "naam", aap.getNaam());
            writeSimpleElement(xmlStreamWriter, "gewicht", String.valueOf(aap.getGewicht()));
            writeSimpleElement(xmlStreamWriter, "geboorte", aap.getGeboorte().toString());
            writeSimpleElement(xmlStreamWriter, "kooi", aap.getKooi());
            xmlStreamWriter.writeEndElement();
        }
        xmlStreamWriter.writeEndElement();//soort
    }
		xmlStreamWriter.writeEndElement();//apen
		xmlStreamWriter.writeEndDocument();
		xmlStreamWriter.close();
		writerXml.close();

		System.out.println("File saved!");
}

    private static void writeSimpleElement(
            XMLStreamWriter xmlStreamWriter,
            String naam,
            String inhoud
    )
            throws XMLStreamException {
        xmlStreamWriter.writeStartElement(naam);
        xmlStreamWriter.writeCharacters(inhoud);
        xmlStreamWriter.writeEndElement();
    }

     */
}
