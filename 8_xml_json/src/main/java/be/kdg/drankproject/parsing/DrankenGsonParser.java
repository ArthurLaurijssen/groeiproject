package be.kdg.drankproject.parsing;

import be.kdg.drankproject.model.Dranken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.ietf.jgss.GSSName;

import java.io.*;

public class DrankenGsonParser {
    public static void writeJson(Dranken dranken, String fileName) {
        GsonBuilder builder = new GsonBuilder();
        Gson gs = builder.setPrettyPrinting().create();
        String jsonStri = gs.toJson(dranken);

        try (FileWriter jsonWriter = new FileWriter(fileName)) {
            jsonWriter.write(jsonStri);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Dranken readJson(String fileName) {
        Dranken dranken = new Dranken();
        try {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();

            BufferedReader dataReader = new BufferedReader(new FileReader(fileName));

            Gson gs = builder.setPrettyPrinting().create();
            dranken =  gs.fromJson(dataReader,Dranken.class);
            return dranken;
        }catch (IOException e) {
            e.printStackTrace();
        }

        return dranken;
    }


}
