package be.kdg.drankproject.parsing;


import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;


import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class DrankenDomParser {
    public static Dranken domReadXml(String fileName) {
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(fileName));


            //Extract the root element
            Element el = doc.getDocumentElement();
            NodeList personNodes = el.getChildNodes();
            XMLInputFactory fac = XMLInputFactory.newInstance();
            XMLEventReader eventReader = fac.createXMLEventReader(new FileReader(fileName));
            Dranken dranken = new Dranken();
            Drank drank = null;
            String dataName="";
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType()) {
                    case XMLStreamConstants.START_ELEMENT:
                        drank = new Drank();
                        StartElement startElement = event.asStartElement();

                        String tagName = startElement.getName().getLocalPart();
                        if (tagName.equals("drank")) {
                            Attribute categorie = startElement.getAttributeByName(new QName("categorie"));
                            drank.setCategorie(Categorie.valueOf(categorie.getValue()));
                        }
                        else {
                            dataName=tagName;
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();
                        switch (dataName) {
                            case "naam":
                                drank.setNaam(characters.getData());
                                break;
                            case "herkomst":
                                drank.setHerkomst(characters.getData());
                                break;
                            case "barcode":
                                drank.setBarcode(characters.getData());
                                break;
                            case "inhoud":
                                drank.setInhoud(Double.parseDouble(characters.getData()));
                                break;
                            case "verpaktper":
                                drank.setVerpaktPer(Integer.parseInt(characters.getData()));
                                break;
                            case "uitvindingsdatum":
                                drank.setUitvindingDatum(LocalDate.parse(characters.getData()));
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();
                        if (endElement.getName().getLocalPart().equals("drank")) {
                            dranken.voegToe(drank);
                        }
                        break;
                }
                return dranken;
            }

        }catch (IOException | XMLStreamException | ParserConfigurationException | SAXException e) { {
            e.printStackTrace();
        }

        }
        return null;
    }
}
