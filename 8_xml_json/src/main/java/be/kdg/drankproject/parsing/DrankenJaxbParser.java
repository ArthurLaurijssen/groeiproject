package be.kdg.drankproject.parsing;

import javax.xml.bind.*;
import java.io.File;

public class DrankenJaxbParser {
    public static void JaxbWriteXml(String file, Object root) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(root.getClass());
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(root,new File(file));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
    public static <T> T JaxbReadXml(String file, Class<T> typeParameterClass) {
        try {
            JAXBContext context = JAXBContext.newInstance(typeParameterClass);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            T object = (T) unmarshaller.unmarshal(new File(file));
            return object;
        } catch (JAXBException e) {
        e.printStackTrace();
        }
        return null;
    }

}
