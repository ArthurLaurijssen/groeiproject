package be.kdg.drankproject.parsing;


import be.kdg.drankproject.data.Data;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class ParserTest {

    private Dranken dranken;




    @Before
    public void setUp() throws Exception {
        dranken = new Dranken();
        for (Drank drank : Data.getData()) {
            dranken.voegToe(drank);
        }
    }

    @Test
    public void testStaxDom() {
        DrankenStaxParser drankenStaxParser = new DrankenStaxParser(dranken,"datafiles/dranken.xml");
        try {
            drankenStaxParser.WriteXML();
        } catch (XMLStreamException e) {
            e.printStackTrace();
            fail("XmlStreamException");
        } catch (IOException e) {
            e.printStackTrace();
            fail("File exception");
        }
        Dranken drankenAfterXML = DrankenDomParser.domReadXml("datafiles/dranken.xml");
        int count = 0;
        for (Drank drank :drankenAfterXML.gesorteerdOpNaam()) {
            assertEquals("Object zou hetzelfde moeten zijn",drank,dranken.gesorteerdOpNaam().get(count++));
        }
    }

    @Test
    public void testJaxb() {
        DrankenJaxbParser.JaxbWriteXml("datafiles/drankenJAX.xml",dranken);
        Dranken drankenAfterXML = DrankenJaxbParser.JaxbReadXml("datafiles/drankenJAX.xml",Dranken.class);
        int count = 0;
        for (Drank drank :drankenAfterXML.gesorteerdOpNaam()) {
            assertEquals("Object zou hetzelfde moeten zijn",drank,dranken.gesorteerdOpNaam().get(count++));
        }
    }

    @Test
    public void testGson() {
        DrankenGsonParser.writeJson(dranken,"datafiles/drankenGSON.json");
        Dranken drankenAfterXML = DrankenGsonParser.readJson("datafiles/drankenGSON.json");
        int count = 0;
        for (Drank drank :drankenAfterXML.gesorteerdOpNaam()) {
            assertEquals("Object zou hetzelfde moeten zijn",drank,dranken.gesorteerdOpNaam().get(count++));
        }
    }
}


