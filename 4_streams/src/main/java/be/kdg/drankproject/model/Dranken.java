package be.kdg.drankproject.model;

import java.util.*;
import java.util.function.Function;

public class Dranken {
    Set<Drank> dranken;

    public Dranken() {
        this.dranken = new TreeSet<>();
    }

    public boolean voegToe(Drank drank) {
        for (Drank d1 : dranken) {
            if (d1.equals(drank)) {
                return false;
            }
        }
        dranken.add(drank);
        return true;
    }

    public boolean verwijder(String naam) {
        for (Iterator<Drank> it = dranken.iterator(); it.hasNext(); ) {
            Drank drank = (Drank) it.next();
            if (drank.getNaam().equals(naam)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    public Drank zoek(String naam) {
        for (Iterator<Drank> it = dranken.iterator(); it.hasNext(); ) {
            Drank drank = (Drank) it.next();
            if (drank.getNaam().equals(naam)) {
                return drank;
            }
        }
        return null;
    }

    public List<Drank> sortedBy(Function<Drank,Comparable> f) {
        List<Drank> list = new ArrayList<>(this.dranken);
        Collections.sort(list,(Comparator<Drank>) (c1,c2) -> f.apply(c1).compareTo(f.apply(c2)));
        return list;
    }
    public int getAantal() {
        return Drank.getAantal();
    }
}
