package be.kdg.drankproject.data;

import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.model.Drank;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Data {


    public static List<Drank> getData() {
        List<Drank> list = new ArrayList<Drank>();
        list.add(new Drank("Coca Cola" , "USA", Categorie.FRISDRANK, LocalDate.of(1950,10,25),33.0,6,"12445678610"));
        list.add(new Drank("Fanta" , "USA", Categorie.FRISDRANK, LocalDate.of(1970,11,15),33.0,6,"12342178610"));
        list.add(new Drank("Sprite" , "Belgium", Categorie.FRISDRANK, LocalDate.of(1960,9,18),33.0,6,"12345678610"));
        list.add(new Drank("Almdudler" , "Germany", Categorie.FRISDRANK, LocalDate.of(1935,6,20),33.0,6,"14345678610"));
        list.add(new Drank("7-UP" , "USA", Categorie.FRISDRANK, LocalDate.of(1969,10,25),33.0,6,"12345678210"));

        list.add(new Drank("Stella Artois" , "Belgium", Categorie.ALCOHOL, LocalDate.of(1956,10,25),25.0,6,"1234"));
        list.add(new Drank("Jupiler" , "Belgium", Categorie.ALCOHOL, LocalDate.of(1957,3,25),25.0,6,"1234388848610"));
        list.add(new Drank("Heineken" , "Netherlands", Categorie.ALCOHOL, LocalDate.of(1930,2,25),25.0,1,"123453827723678610"));
        list.add(new Drank("Grolsch" , "Netherlands", Categorie.ALCOHOL, LocalDate.of(1980,9,25),25.0,6,"62345678610"));
        list.add(new Drank("Duvel" , "Belgium", Categorie.ALCOHOL, LocalDate.of(1934,11,25),25.0,6,"52345678610"));

        list.add(new Drank("Aquarius" , "Belgium", Categorie.SPORTDRANK, LocalDate.of(1944,11,25),50.0,6,"82345678610"));
        list.add(new Drank("AA" , "USA", Categorie.SPORTDRANK, LocalDate.of(1924,10,22),100.0,10,"42345678610"));
        list.add(new Drank("Spa" , "Belgium", Categorie.WATER, LocalDate.of(1968,5,15),100.0,6,"32345678610"));
        list.add(new Drank("Evian" , "France", Categorie.WATER, LocalDate.of(1900,1,31),100.0,6,"22345678610"));
        list.add(new Drank("Chaudfontaine" , "Belgium", Categorie.WATER, LocalDate.of(1999,3,5),100.0,7,"465785859610"));
        return list;
    }
}
