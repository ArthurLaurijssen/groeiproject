package be.kdg;

import be.kdg.drankproject.data.Data;
import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;
import be.kdg.util.Functions;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class Demo_4 {

    public static void main(String[] args) {
        Dranken dranken = new Dranken();

        /*for (Drank drank : Data.getData()) {
            dranken.voegToe(drank);
        }*/
        Data.getData().stream().forEach(drank -> dranken.voegToe(drank));

        System.out.println("\nDranken gesorteerd op naam:");

        dranken.sortedBy(Drank::getNaam).stream().forEach(System.out::println);
        /*for (Drank drank : dranken.sortedBy(Drank::getNaam)) {
            System.out.println(drank);
        }*/
        System.out.println("\n\nDranken gesorteerd op herkomst:");



        /*for (Drank drank : dranken.sortedBy(Drank::getHerkomst)) {
            System.out.println(drank);
        }*/
        dranken.sortedBy(Drank::getHerkomst).stream().forEach(System.out::println);
        System.out.println("\n\n");

        List<Drank> drankenList = Data.getData();


        System.out.println("Toepassing 3 keer FilteredList met telkens een ander Predicate:");


        System.out.println("\nAlle dranken die Fanta noemen");
        drankenList = Functions.filteredList(drankenList,drank -> drank.getNaam().equals("Fanta"));


        drankenList.stream().forEach(System.out::println);
        /*
        for (Drank drank : drankenList) {
            System.out.println(drank.toString());
        }*/


        System.out.println("\nAlle dranken die uit de USA komen en een frisdrank zijn");
        drankenList = Data.getData();
        drankenList = Functions.filteredList(drankenList,drank -> drank.getHerkomst().equals("USA") && drank.getCategorie().equals(Categorie.FRISDRANK));

        drankenList.stream().forEach(System.out::println);
        /*for (Drank drank : drankenList) {
            System.out.println(drank.toString());
        }*/

        System.out.println("\nAlle dranken die uit de Alcohol bevatten en voor 1950 zijn uitgevonden");
        drankenList = Data.getData();
        drankenList = Functions.filteredList(drankenList,drank -> drank.getUitvindingDatum().isBefore(LocalDate.of(1950,01,01)) && drank.getCategorie().equals(Categorie.ALCOHOL));


        drankenList.stream().forEach(System.out::println);
        /*for (Drank drank : drankenList) {
            System.out.println(drank.toString());
        }
        */


        System.out.println("\n\nAverage function:");

        drankenList = Data.getData();

        System.out.printf("Gemiddeld aantal inhoud: %.1f ml\n", Functions.average (drankenList, Drank::getInhoud));


        System.out.println("\n\n");
        System.out.println("Aantal function:");
        System.out.printf("\nAantal dranken uit Belgie: %d",Functions.countIf(drankenList, drank -> drank.getHerkomst().equals("Belgium")));
        System.out.printf("\nAantal frisdranken: %d",Functions.countIf(drankenList, drank -> drank.getCategorie().equals(Categorie.FRISDRANK)));


        System.out.println("\n\nStreams:\n");

        List<Drank> drankenListStreams = Data.getData();
        System.out.printf("Aantal dranken dat Alcohol bevat: %d",drankenListStreams.stream().filter(drank -> drank.getCategorie().equals(Categorie.ALCOHOL)).count());

        System.out.println("\nAlle dranken gesorteerd op herkomst en dan op naam");
        drankenListStreams.stream().sorted(Comparator.comparing(Drank::getHerkomst).thenComparing(Drank::getNaam )).forEach(System.out::println);

        System.out.println("\nAlle landen van herkomst in hoofdletters, omgekeerd gesorteerd en zonder dubbels:");
        System.out.println(drankenListStreams.stream().sorted(Comparator.comparing(Drank::getHerkomst).reversed()).map(Drank::getHerkomst).distinct().collect(Collectors.joining(" , ")).toUpperCase());

        /*
        Een willekeurige dictator met meer dan 75% wreedheid: Mao Tse-tung : 80.0
         */
        System.out.println("\n\nEen willekeurige drank van Belgie: ");
        Drank dran = drankenListStreams.stream().filter(drank -> drank.getHerkomst().equals("Belgium")).findAny().get();
        System.out.print(dran);



        //De dictator met de meeste slachtoffers:
        System.out.print("\n\nDe drank met het meeste ml: ");
        drankenListStreams.stream().max(Comparator.comparing(Drank::getInhoud)).ifPresent(System.out::print);
        /*
        List met gesorteerde dictatornamen die beginnen met 'A': [Adolf Hitler, Augusto Pinochet, Ayatollah Khomeini]
         */
        System.out.println("\n\nList met gesorteerd drankennamen die beginnen met een 'S': ");
        System.out.print(drankenListStreams.stream().filter(drank -> drank.getNaam().substring(0,1).equals("S")).map(Drank::getNaam).collect(Collectors.toList()).toString());




        /*
        Sublist met dictators geboren VOOR 1900:
         */
        Map<Boolean,List<Drank>> drankenGesorteerdPer = drankenListStreams.stream().collect(Collectors.partitioningBy(drank -> drank.getUitvindingDatum().isBefore(LocalDate.of(1950,01,01))));
        System.out.println("\n\nSublist met dranken voor 1950:");
        drankenGesorteerdPer.get(true).stream().forEach(System.out::println);
        System.out.println("\nSublist met dranken na 1950");
        drankenGesorteerdPer.get(false).stream().forEach(System.out::println);
    }

}
