package be.kdg.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class Functions {

    public static <T> List<T> filteredList(List<T> drankenList, Predicate<T> predicate) {
        List<T> filteredListMade = drankenList.stream().filter(predicate).collect(Collectors.toList());
        return filteredListMade;


        /*for (T drank : drankenList ) {
           if (predicate.test(drank)) {
               filteredListMade.add(drank);
           }
        }*/
    }

    public static <T> Double average (List<T> drankenList, ToDoubleFunction<T> mapper) {
        return drankenList.stream().mapToDouble(mapper).average().getAsDouble();
        /*
        Double average = 0.0;
        int count = 0;
        for (T drank : drankenList) {
            average += mapper.applyAsDouble(drank);
            count++;
        }
        return average/count;*/
    }

    public static <T> long countIf(List<T> drankenList,Predicate<T> predicate) {
        return drankenList.stream().filter(predicate).count();
        /*
        long count = 0;
        for (T drank : drankenList ) {
            if (predicate.test(drank)) {
                count++;
            }
        }
        return count;*/
    }

}
