package be.kdg.drankproject;

import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;

import java.time.LocalDate;

public class Demo_6 {
    /*
      Maak daar een aantal foutieve basisobjecten aan om de logging te testen.
     */
    public static void main(String[] args) {
        Drank drank1 = new Drank("Coca Cola" , "USA", Categorie.FRISDRANK, LocalDate.of(2030,10,25),33.0,6,"12445678610");
        Drank drank2 =new Drank("" , "USA", Categorie.FRISDRANK, LocalDate.of(1970,11,15),33.0,6,"12342178610");
        Drank drank3 =new Drank("Sprite" , "Belgium", Categorie.FRISDRANK, LocalDate.of(1960,9,18),33.0,6,"");
        Drank drank4 =new Drank("Almdudler" , "Germany", Categorie.FRISDRANK, LocalDate.of(1935,6,20),33.0,-1,"14345678610");
        Dranken dranken = new Dranken();
        dranken.voegToe(drank1);
        dranken.voegToe(drank2);
        dranken.voegToe(drank3);
        dranken.verwijder(drank1.getNaam());
    }


}
