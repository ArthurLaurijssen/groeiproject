package be.kdg.drankproject.model;

import be.kdg.drankproject.Demo_6;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Drank implements Comparable<Drank>{
    private static Logger logger;


    private String naam,herkomst,barcode;
    private Categorie categorie;
    private LocalDate uitvindingDatum;
    private double inhoud;
    private int verpaktPer;
    private static int aantal = 0;

    public Drank() {
        registerLogger();
        this.naam = "ongekent";
        this.herkomst = "anoniem";
        this.categorie = Categorie.ONGEKENT;
        this.uitvindingDatum = LocalDate.of(LocalDate.now().getYear(),LocalDate.now().getMonth(),LocalDate.now().getDayOfMonth()-1);
        this.inhoud = 0.0;
        this.verpaktPer = 0;
        this.barcode = "012345678910";
        aantal++;
    }
    public Drank(String naam, String herkomst, Categorie categorie, LocalDate uitvindingDatum, double inhoud, int verpaktPer,String barcode) {
        registerLogger();
        this.setNaam(naam);
        this.setHerkomst(herkomst);
        this.setCategorie(categorie);
        this.setUitvindingDatum(uitvindingDatum);
        this.setInhoud(inhoud);
        this.setVerpaktPer(verpaktPer);
        this.setBarcode(barcode);
        aantal++;
    }
    private void registerLogger() {
        System.setProperty("java.util.logging.config.file",
                "6_logging/src/main/resources/logging.properties");
        //must initialize loggers after setting above property
        logger = Logger.getLogger("be.be.kdg.drankproject.model.drank");
        try {
            FileHandler fileHandler = new FileHandler("log/logDrank.log", true);
            logger.addHandler(fileHandler);
            logger.setLevel(Level.SEVERE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setNaam(String naam) {
        if (naam.isEmpty()) {
            this.logger.severe("'" + naam + "' naam mag niet leeg zijn ");
        }
        this.naam = naam;
    }

    public void setHerkomst(String herkomst) {
        if (herkomst.isEmpty()) {
            logger.severe(this.naam + "'s herkomst (" + herkomst + ") mag niet leeg zijn");
        }
        this.herkomst = herkomst;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public void setUitvindingDatum(LocalDate uitvindingDatum) {
        if (!uitvindingDatum.isBefore(LocalDate.now()))  {
            logger.severe(this.naam + "'s uitvingsdatum (" + uitvindingDatum + ") ligt in de toekomst");
        }
        this.uitvindingDatum= uitvindingDatum;
    }

    public void setInhoud(double inhoud) {
        if (inhoud<=0) {
            logger.severe(this.naam + "'s inhoud (" + inhoud + ") mag niet negatief zijn");
        }
        this.inhoud = inhoud;
    }

    public void setVerpaktPer(int verpaktPer) {

            if (verpaktPer<=0) {
                logger.severe(this.naam + "'s verpakt per hoeveelheid (" + verpaktPer + ") mag niet negatief zijn");
            }
            this.inhoud = inhoud;
    }

    public void setBarcode(String barcode) {

            if (barcode.isEmpty()) {
                logger.severe(this.naam + "'s barcode (" + barcode + ") mag niet leeg zijn");
            }
            this.barcode = barcode;
    }

    public String getNaam() {
        return naam;
    }

    public String getHerkomst() {
        return herkomst;
    }

    public String getBarcode() {
        return barcode;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public LocalDate getUitvindingDatum() {
        return uitvindingDatum;
    }

    public double getInhoud() {
        return inhoud;
    }

    public int getVerpaktPer() {
        return verpaktPer;
    }

    public static int getAantal() {
        return aantal;
    }

    @Override
    public int hashCode() {
        return this.barcode.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Drank object = (Drank) obj;
        if (object.barcode.equals(this.barcode)) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Drank drank) {
        return this.getBarcode().compareTo(drank.getBarcode());
    }
    @Override
    public String toString() {
        return String.format("%-15s (%s) \t %-11s  %s",this.naam,this.uitvindingDatum.getYear(),this.herkomst,this.categorie.toString());
    }

}
