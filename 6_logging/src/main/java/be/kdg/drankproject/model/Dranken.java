package be.kdg.drankproject.model;

import java.io.IOException;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Dranken {

    private static Logger logger;



    Set<Drank> dranken;

    public Dranken() {
        this.dranken = new TreeSet<>();
        registerLogger();
    }
    private void registerLogger() {
        System.setProperty("java.util.logging.config.file", "src/main/resources/logging.properties");
        //must initialize loggers after setting above property
        logger = Logger.getLogger("be.be.kdg.drankproject.model.drank");
        try {
            FileHandler fileHandler = new FileHandler("log/logDranken.log", true);
            logger.addHandler(fileHandler);
            logger.setLevel(Level.FINER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean voegToe(Drank drank) {
        for (Drank d1 : dranken) {
            if (d1.equals(drank)) {
                return false;
            }
        }
        dranken.add(drank);
        logger.finer(drank.toString() + " toegevoegd aan de lijst");
        return true;
    }

    public boolean verwijder(String naam) {
        for (Iterator<Drank> it = dranken.iterator(); it.hasNext(); ) {
            Drank drank = (Drank) it.next();
            if (drank.getNaam().equals(naam)) {
                it.remove();
                logger.finer(it.toString() + " verwijderd uit de lijst");
                return true;
            }
        }
        return false;
    }

    public Drank zoek(String naam) {
        for (Iterator<Drank> it = dranken.iterator(); it.hasNext(); ) {
            Drank drank = (Drank) it.next();
            if (drank.getNaam().equals(naam)) {
                return drank;
            }
        }
        return null;
    }
    public List<Drank> gesorteerdOpNaam() {
        List<Drank> list = new ArrayList<>(this.dranken);

        Collections.sort(list, new Comparator<Drank>() {
            @Override
            public int compare(Drank u1, Drank u2) {
                return u1.getNaam().compareTo(u2.getNaam());
            }
        });
        return list;
    }
    public List<Drank> gesorteerdOpDatum() {
        List<Drank> list = new ArrayList<>(this.dranken);
        Collections.sort(list, new Comparator<Drank>() {
            @Override
            public int compare(Drank drank, Drank t1) {
                return drank.getUitvindingDatum().compareTo(t1.getUitvindingDatum());
            }
        });
        return list;
    }
    public List<Drank> gesorteerdOpHerkomst(){
        List<Drank> list = new ArrayList<>(this.dranken);
        Collections.sort(list, new Comparator<Drank>() {
            @Override
            public int compare(Drank drank, Drank t1) {
                return drank.getHerkomst().compareTo(t1.getHerkomst());
            }
        });
        return list;
    }
    public int getAantal() {
        return Drank.getAantal();
    }
}
