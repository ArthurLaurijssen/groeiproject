package be.kdg.drankproject.model;



import java.time.LocalDate;
import java.util.Random;

public class DrankFactory {
    private DrankFactory() {
    }

    public static Drank newEmptyDrank() {
        return new Drank();
    }
    public static Drank newFilledDrank(String naam, String herkomst, Categorie categorie, LocalDate uitvindingDatum, double inhoud, int verpaktPer, String barcode) {
        return new Drank(naam,herkomst,categorie,uitvindingDatum,inhoud,verpaktPer,barcode);
    }

    public static Drank newRandomDrank() {
        String naam,herkomst,barcode;
        Categorie categorie;
        LocalDate uitvindingDatum;
        double inhoud;
        int verpaktPer;

        naam = generateString(8,2,true);
        herkomst = generateString(8,1,true);
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i =0;i<12;i++) {
            stringBuilder.append(random.nextInt(10));
        }
        barcode = stringBuilder.toString();
        //Letter.values()[new Random().nextInt(Letter.values().length)];
        categorie = Categorie.values()[new Random().nextInt(Categorie.values().length)];
        uitvindingDatum = LocalDate.of(random.nextInt(60)+1950,random.nextInt(12)+1,random.nextInt(27)+1);
        inhoud = random.nextInt(100)+1;
        verpaktPer = random.nextInt(20)+1;
        return new Drank(naam,herkomst,categorie,uitvindingDatum,inhoud,verpaktPer,barcode);
    }


    private static String generateString(int maxWordLength, int wordCount, boolean camelCase) {
        char[] medeklinkers = "bcdfghjklmnpqrstvwxyz".toCharArray();
        char[] medeklinkersUpper = "BCDFGHIJKLMNPQRSTVWXYZ".toCharArray();
        char[] klinkers = "aeiou".toCharArray();
        char[] klinkersUpper = "AEIOU".toCharArray();
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        boolean firstchar = false;
        for (int i = 0;i< wordCount ;i++) {
            if (i != 0) {
                builder.append(" ");
            }
            for (int j = 0; j < maxWordLength; j++) {
                if (j == 0 && camelCase) {
                    firstchar = true;
                }
                int resultKlinkerOrMede = random.nextInt(3);
                if (resultKlinkerOrMede % 2 == 0) {
                    //medeklinker
                    int result2 = random.nextInt(medeklinkers.length);
                    if (firstchar) {
                        builder.append(medeklinkersUpper[result2]);
                        firstchar=false;
                    } else {
                        builder.append(medeklinkers[result2]);
                    }
                } else {
                    //klinker
                    int result2 = random.nextInt(klinkers.length);
                    if (firstchar) {
                        builder.append(klinkersUpper[result2]);
                        firstchar=false;
                    } else {
                        builder.append(klinkers[result2]);
                    }
                }
            }
        }
        return builder.toString();
    }
}
