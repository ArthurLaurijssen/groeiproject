package be.kdg.drankproject.model;

import java.time.LocalDate;

public final class Drank implements Comparable<Drank>{
    private final String naam,herkomst,barcode;
    private final Categorie categorie;
    private final LocalDate uitvindingDatum;
    private final double inhoud;
    private final int verpaktPer;
    private static int aantal = 0;

    public Drank() {
        this.naam = "ongekent";
        this.herkomst = "anoniem";
        this.categorie = Categorie.ONGEKENT;
        this.uitvindingDatum = LocalDate.of(LocalDate.now().getYear(),LocalDate.now().getMonth(),LocalDate.now().getDayOfMonth()-1);
        this.inhoud = 0.0;
        this.verpaktPer = 0;
        this.barcode = "012345678910";
        aantal++;
    }
    public Drank(String naam, String herkomst, Categorie categorie, LocalDate uitvindingDatum, double inhoud, int verpaktPer,String barcode) {
        this.naam = naam;
        this.herkomst = herkomst;
        this.categorie = categorie;
        this.uitvindingDatum =uitvindingDatum;
        this.inhoud = inhoud;
        this.verpaktPer = verpaktPer;
        this.barcode = barcode;
        aantal++;
    }

    public String getNaam() {
        return naam;
    }

    public String getHerkomst() {
        return herkomst;
    }

    public String getBarcode() {
        return barcode;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public LocalDate getUitvindingDatum() {
        return uitvindingDatum;
    }

    public double getInhoud() {
        return inhoud;
    }

    public int getVerpaktPer() {
        return verpaktPer;
    }

    public static int getAantal() {
        return aantal;
    }

    @Override
    public int hashCode() {
        return this.barcode.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Drank object = (Drank) obj;
        if (object.barcode.equals(this.barcode)) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Drank drank) {
        return this.getBarcode().compareTo(drank.getBarcode());
    }
    @Override
    public String toString() {
        return String.format("%-15s (%s) \t %-11s  %s",this.naam,this.uitvindingDatum.getYear(),this.herkomst,this.categorie.toString());
    }

}
