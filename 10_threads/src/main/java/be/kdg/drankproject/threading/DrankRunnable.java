package be.kdg.drankproject.threading;

import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.DrankFactory;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DrankRunnable implements Runnable{
    private Predicate<Drank> predicate;
    private List<Drank> drankList;
    public DrankRunnable(Predicate<Drank> predicate) {
        this.predicate =predicate;
    }

    @Override
    public void run() {
        //System.out.println("Started thread:" + Thread.currentThread().getName());
        Supplier<Drank> drankSupplier = () -> DrankFactory.newRandomDrank();
        this.drankList = (List<Drank>) Stream.generate(drankSupplier).filter(this.predicate).limit(1000).collect(Collectors.toList());
        //System.out.println("Finished thread:" + Thread.currentThread().getName());
    }

    public List<Drank> getDrankList() {
        return drankList;
    }
}
