package be.kdg.drankproject.threading;

import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.DrankFactory;
import com.sun.java.accessibility.util.internal.ListTranslator;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DrankCallable implements Callable<List<Drank>> {
    private Predicate<Drank> predicate;

    public DrankCallable(Predicate<Drank> predicate) {
        this.predicate = predicate;
    }

    @Override
    public List<Drank> call() throws Exception {
        Supplier<Drank> drankSupplier = () -> DrankFactory.newRandomDrank();
        return (List<Drank>) Stream.generate(drankSupplier).filter(this.predicate).limit(1000).collect(Collectors.toList());
    }
}
