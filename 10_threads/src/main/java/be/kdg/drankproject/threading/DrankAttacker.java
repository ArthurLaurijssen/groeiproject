package be.kdg.drankproject.threading;

import be.kdg.drankproject.model.Drank;

import java.util.List;
import java.util.function.Predicate;

public class DrankAttacker implements Runnable {
    private static final Object LOCK = new Object();
    private List<Drank> drankList;
    private Predicate<Drank> drankPredicate;

    public DrankAttacker(List<Drank> drankList, Predicate<Drank> drankPredicate) {
        this.drankList = drankList;
        this.drankPredicate = drankPredicate;
    }

    @Override
    public void run() {
        synchronized (LOCK) {
            drankList.removeIf(drankPredicate);
        }
    }
}
