package be.kdg.drankproject;

import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.threading.DrankRunnable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Demo_10 {
    private static List<Long> testcount = new ArrayList<>();
    public static void main(String[] args) throws InterruptedException {
        List<DrankRunnable> drankRunnables = new ArrayList<>();
        List<Thread> threads = new ArrayList<>();


        for (int j = 0;j<100;j++) {
            drankRunnables.clear();
            threads.clear();

            drankRunnables.add(new DrankRunnable(drank -> drank.getCategorie().equals(Categorie.ALCOHOL)));
            drankRunnables.add(new DrankRunnable(drank -> drank.getNaam().substring(0,1).equals("A")));
            drankRunnables.add(new DrankRunnable(drank -> drank.getUitvindingDatum().isBefore(LocalDate.of(1970,10,6))));
            threads.add(new Thread(drankRunnables.get(0)));
            threads.add(new Thread(drankRunnables.get(1)));
            threads.add(new Thread(drankRunnables.get(2)));


            long time = System.currentTimeMillis();
            for (int i = 0 ; i<threads.size();i++) {
                threads.get(i).start();
            }
            for (int i = 0 ; i<threads.size();i++) {
                threads.get(i).join();
            }
            long timedifference = System.currentTimeMillis()-time;
            testcount.add(timedifference);
        }

        for (int i = 0 ; i<drankRunnables.size();i++) {
            System.out.println("\nLijst dranken: \n");
            drankRunnables.get(i).getDrankList().stream().limit(5).forEach(System.out::println);
        }


        System.out.println("\n\n4 threads verzamelen elk 1000 dranken (gemiddelde uit 100 runs): " + testcount.stream().mapToInt(Long::intValue).average().getAsDouble() + "ms");




    }

}
