package be.kdg.drankproject;

import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.DrankFactory;
import be.kdg.drankproject.threading.DrankAttacker;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_11 {
    List<Drank> drankList;
    private synchronized List<Drank> getList() {
        return null;
    }
    public static void main(String[] args) {

        Supplier<Drank> drankSupplier = () -> DrankFactory.newRandomDrank();
        List<Drank> drankList = (List<Drank>) Stream.generate(drankSupplier).limit(1000).collect(Collectors.toList());



        List<Predicate<Drank>> drankPredicates = new ArrayList<>();
        drankPredicates.add(drank -> drank.getCategorie().equals(Categorie.ALCOHOL));
        drankPredicates.add(drank -> drank.getUitvindingDatum().isBefore(LocalDate.of(2010,1,1)));
        drankPredicates.add(drank -> drank.getNaam().contains("ac"));

        List<Thread> attackers = new ArrayList<>();
        for (Predicate<Drank> predicate : drankPredicates) {
            attackers.add(new Thread(new DrankAttacker(drankList,predicate)));
        }

        for (Thread attackerThread : attackers) {
            attackerThread.start();
        }
        for (Thread attackerThread : attackers) {
            try {
                attackerThread.join();
            } catch (InterruptedException e) {
            }

        }
        System.out.println("Na uitzuivering: ");
        System.out.println("Aantal dranken met 'AC' in hun naam:" + drankList.stream().filter(drankPredicates.get(2)).count());
        System.out.println("Aantal dranken voor 2010: " + drankList.stream().filter(drankPredicates.get(1)).count());
        System.out.println("Aantal alcoholische dranken: "+ drankList.stream().filter(drankPredicates.get(0)).count());

    }
}
