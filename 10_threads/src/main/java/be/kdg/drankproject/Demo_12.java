package be.kdg.drankproject;

import be.kdg.drankproject.model.Categorie;
import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.threading.DrankCallable;
import be.kdg.drankproject.threading.DrankRunnable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Demo_12 {
    private static List<Long> testcount = new ArrayList<>();
    public static void main(String[] args) {
        List<DrankCallable> drankCallables = new ArrayList<>();


        for (int i =0;i<100;i++) {
            long time = System.currentTimeMillis();
            drankCallables.clear();
            drankCallables.add(new DrankCallable(drank -> drank.getCategorie().equals(Categorie.ALCOHOL)));
            drankCallables.add(new DrankCallable(drank -> drank.getNaam().substring(0,1).equals("A")));
            drankCallables.add(new DrankCallable(drank -> drank.getUitvindingDatum().isBefore(LocalDate.of(1970,10,6))));
            ExecutorService pool = Executors.newFixedThreadPool(3);
            Set<Future<List<Drank>>> futures = new HashSet<>();

            for (DrankCallable drankCallable:drankCallables) {
                Future<List<Drank>> future = pool.submit(drankCallable);
                futures.add(future);
            }
            pool.shutdown();
            long timedifference = System.currentTimeMillis()-time;
            testcount.add(timedifference);
        }
        System.out.println("\n\n3 Futures verzamelen elk 1000 dranken (gemiddelde uit 100 runs): " + testcount.stream().mapToInt(Long::intValue).average().getAsDouble() + "ms");



    }
}
