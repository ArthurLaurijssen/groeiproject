package be.kdg.patterns;

import be.kdg.model.Drank;
import be.kdg.model.Dranken;
import be.kdg.model.DrankenInterface;

import java.util.List;
import java.util.Observable;

public class ObservableDranken extends Observable implements DrankenInterface {
    private Dranken dranken;

    public ObservableDranken(Dranken dranken) {
        this.dranken = dranken;
    }


    @Override
    public boolean voegToe(Drank drank) {
        setChanged();
        notifyObservers(drank);
        return dranken.voegToe(drank);
    }

    @Override
    public boolean verwijder(String naam) {
        setChanged();
        notifyObservers(naam);
        return dranken.verwijder(naam);
    }

    @Override
    public Drank zoek(String naam) {
        return dranken.zoek(naam);
    }

    @Override
    public List<Drank> gesorteerdOpNaam() {
        return dranken.gesorteerdOpNaam();
    }

    @Override
    public List<Drank> gesorteerdOpDatum() {
        return dranken.gesorteerdOpDatum();
    }

    @Override
    public List<Drank> gesorteerdOpHerkomst() {
        return dranken.gesorteerdOpHerkomst();
    }

    @Override
    public int getAantal() {
        return dranken.getAantal();
    }
}
