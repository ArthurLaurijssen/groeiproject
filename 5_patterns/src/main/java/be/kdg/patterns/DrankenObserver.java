package be.kdg.patterns;

import be.kdg.model.Drank;
import be.kdg.model.Dranken;

import java.util.Observable;
import java.util.Observer;

public class DrankenObserver implements Observer {
    private Dranken dranken;

    public DrankenObserver(Dranken dranken) {
        this.dranken = dranken;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Drank) {
            System.out.printf("\nObserver meld: toegevoegd (%s)",arg.toString());

        } else {
            System.out.printf("\nObserver meldt: verwijderd (%s)",dranken.zoek(arg.toString()));
        }

    }

    /*
    Observer meldt: Toegevoegd: ... (toString van toegevoegd object) ... OF:
Observer meldt: Verwijderd: ... (toString van verwijderd object) ...

     */

}
