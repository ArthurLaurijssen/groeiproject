package be.kdg.model;

import java.util.List;

public interface DrankenInterface {
    boolean voegToe(Drank drank);

    boolean verwijder(String naam);

    Drank zoek(String naam);

    List<Drank> gesorteerdOpNaam();

    List<Drank> gesorteerdOpDatum();

    List<Drank> gesorteerdOpHerkomst();

    int getAantal();
}
