package be.kdg.model;

import java.util.*;

public class Dranken implements DrankenInterface {
    Set<Drank> dranken;

    public Dranken() {
        this.dranken = new TreeSet<>();
    }

    @Override
    public boolean voegToe(Drank drank) {
        for (Drank d1 : dranken) {
            if (d1.equals(drank)) {
                return false;
            }
        }
        dranken.add(drank);
        return true;
    }

    @Override
    public boolean verwijder(String naam) {
        for (Iterator<Drank> it = dranken.iterator(); it.hasNext(); ) {
            Drank drank = (Drank) it.next();
            if (drank.getNaam().equals(naam)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    @Override
    public Drank zoek(String naam) {
        for (Iterator<Drank> it = dranken.iterator(); it.hasNext(); ) {
            Drank drank = (Drank) it.next();
            if (drank.getNaam().equals(naam)) {
                return drank;
            }
        }
        return null;
    }
    @Override
    public List<Drank> gesorteerdOpNaam() {
        List<Drank> list = new ArrayList<>(this.dranken);

        Collections.sort(list, new Comparator<Drank>() {
            @Override
            public int compare(Drank u1, Drank u2) {
                return u1.getNaam().compareTo(u2.getNaam());
            }
        });
        return list;
    }
    @Override
    public List<Drank> gesorteerdOpDatum() {
        List<Drank> list = new ArrayList<>(this.dranken);
        Collections.sort(list, new Comparator<Drank>() {
            @Override
            public int compare(Drank drank, Drank t1) {
                return drank.getUitvindingDatum().compareTo(t1.getUitvindingDatum());
            }
        });
        return list;
    }
    @Override
    public List<Drank> gesorteerdOpHerkomst(){
        List<Drank> list = new ArrayList<>(this.dranken);
        Collections.sort(list, new Comparator<Drank>() {
            @Override
            public int compare(Drank drank, Drank t1) {
                return drank.getHerkomst().compareTo(t1.getHerkomst());
            }
        });
        return list;
    }
    @Override
    public int getAantal() {
        return Drank.getAantal();
    }
}
