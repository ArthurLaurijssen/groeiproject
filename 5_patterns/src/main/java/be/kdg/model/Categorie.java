package be.kdg.model;

public enum Categorie {
    ALCOHOL {
        public String toString() {
            return "Alcohol";
        }
    },
    FRISDRANK{
        public String toString() {
            return "Frisdrank";
        }
    },
    SPORTDRANK{
        public String toString() {
            return "Sportdrank";
        }
    },
    WATER{
        public String toString() {
            return "Water";
        }
    },
    ONGEKENT{
        public String toString() {
            return "Ongekent";
        }
    }
}
