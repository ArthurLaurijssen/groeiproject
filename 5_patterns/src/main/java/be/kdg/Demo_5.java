package be.kdg;

import be.kdg.model.Categorie;
import be.kdg.model.Drank;
import be.kdg.model.DrankFactory;
import be.kdg.model.Dranken;
import be.kdg.patterns.DrankenObserver;
import be.kdg.patterns.ObservableDranken;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_5 {
    public static void main(String[] args) {
        Dranken dranken2 = new Dranken();
        ObservableDranken dranken = new ObservableDranken(dranken2);
        DrankenObserver observer = new DrankenObserver(dranken2);
        dranken.addObserver(observer);

        dranken.voegToe(new Drank("Coca Cola" , "USA", Categorie.FRISDRANK, LocalDate.of(1950,10,25),33.0,6,"12445678610"));
        dranken.verwijder("Coca Cola");

        /*Empty dictator:

         */
        System.out.println("\n\nEmpty drank:");
        System.out.println(DrankFactory.newEmptyDrank().toString());

        System.out.println("Filled dictator:");
        System.out.println(DrankFactory.newFilledDrank("Looza Ace","Netherlands",Categorie.FRISDRANK,LocalDate.of(2000,01,23),33,1,"10273737737737"));

        System.out.println("\n30 random dranken gesorteerd op naam:");
        Supplier<Drank> drankSupplier = () -> DrankFactory.newRandomDrank();
        Stream.generate(drankSupplier).limit(30).forEach(System.out::println);
    }
}
