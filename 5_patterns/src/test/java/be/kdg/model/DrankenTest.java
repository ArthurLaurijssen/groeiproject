package be.kdg.model;


import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class DrankenTest {
    Drank drank1,drank2,drank3;
    Dranken dranken;

    @Before
    public void setUp() throws Exception {
        drank1 = new Drank("Fanta", "USA", Categorie.FRISDRANK, LocalDate.of(1970, 11, 15), 33.0, 6, "12342178610");
        drank2 = new Drank("7-UP", "USA", Categorie.FRISDRANK, LocalDate.of(1969, 10, 25), 33.0, 6, "12345678210");
        drank3 = new Drank("Aquarius", "Belgium", Categorie.SPORTDRANK, LocalDate.of(1944, 11, 25), 50.0, 6, "82345678610");
        dranken = new Dranken();
    }

    @Test
    public void testToevoegen() {
        assertTrue("Toevoegen zou true moeten zijn",dranken.voegToe(drank1));

        assertFalse("Dubbele waarden zouden false moeten geven",dranken.voegToe(drank1));
    }
    @Test
    public void testVerwijderen() {
        dranken.voegToe(drank1); // zeker zijn dat drank1 erin staat
        assertFalse("Verwijderen van waarden die niet in de lijst zitten zouden false moeten geven",dranken.verwijder(drank2.getNaam()));
        assertTrue("Drank1 verwijderen zou true moeten geven",dranken.verwijder(drank1.getNaam()));
    }

}
