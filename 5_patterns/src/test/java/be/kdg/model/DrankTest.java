package be.kdg.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;

import static org.junit.Assert.*;


public class DrankTest {

    Drank drank,drank2;

    @Before
    public void setUp() throws Exception {
        drank = new Drank("Fanta" , "USA", Categorie.FRISDRANK, LocalDate.of(1970,11,15),33.0,6,"12342178610");
        drank2 = new Drank("7-UP" , "USA", Categorie.FRISDRANK, LocalDate.of(1969,10,25),33.0,6,"12345678210");
    }

    @Test
    public void testEquals() {
        assertTrue("De 2 dranken moeten gelijk zijn",drank.equals(new Drank("Fanta" , "USA", Categorie.FRISDRANK, LocalDate.of(1970,11,15),33.0,6,"12342178610")));
        assertFalse("De 2 dranken mogen niet gelijk zijn",drank2.equals(drank));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalName() {
        drank.setNaam("");
        fail("Leeg zou een foutmelding moeten geven");
    }

    @Test
    public void testLegalName() {
        try {
            drank.setNaam("Fanta");
        } catch (IllegalArgumentException e) {
            fail("Geen error verwacht bij Fanta");
        }
    }
    @Test
    public void testCompareTo() {
        assertTrue("Drank vs drank2 expected negative number",drank.compareTo(drank2)<0);
        assertTrue("Drank2 vs drank expected positive number",drank2.compareTo(drank)>0);
        assertEquals("Drank vs drank2 expected 0",0,drank.compareTo(drank));
    }

    @Test
    public void testAssertEquals() {
        assertEquals("Waarden kloppen niet",0.2,0.2,0.0);
    }
}
