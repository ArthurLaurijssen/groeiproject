package be.kdg.drankproject.model;

import be.kdg.drankproject.reflection.CanRun;

import java.time.LocalDate;

public class Vloeistof {
    protected String naam;
    protected String herkomst;
    protected LocalDate uitvindingDatum;


    @CanRun("Water")
    public void setNaam(String naam) {
        try {
            if (naam.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.naam = naam;
        } catch(IllegalArgumentException e) {
            System.out.println("Naam mag niet leeg zijn");
        }
    }
    @CanRun("België")
    public void setHerkomst(String herkomst) {
        try {
            if (herkomst.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.herkomst = herkomst;
        } catch(IllegalArgumentException e) {
            System.out.println("Herkomst kan niet leeg zijn");
        }
    }
    @CanRun
    public void setUitvindingDatum(LocalDate uitvindingDatum) {
        try {
            if (!uitvindingDatum.isBefore(LocalDate.now()))  {
                throw new IllegalArgumentException();
            }
            this.uitvindingDatum= uitvindingDatum;
        } catch(IllegalArgumentException e) {
            System.out.println("Kan niet in de toekomst zijn");
        }
    }

    public String getNaam() {
        return naam;
    }

    public String getHerkomst() {
        return herkomst;
    }

    public LocalDate getUitvindingDatum() {
        return uitvindingDatum;
    }
}
