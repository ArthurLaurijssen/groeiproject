package be.kdg.drankproject.model;

import be.kdg.drankproject.reflection.CanRun;

import java.time.LocalDate;

public class Drank extends Vloeistof implements Comparable<Drank>{
    protected String barcode;
    private Categorie categorie;
    private double inhoud;
    private int verpaktPer;
    private static int aantal = 0;

    public Drank() {
        this.naam = "ongekent";
        this.herkomst = "anoniem";
        this.categorie = Categorie.ONGEKENT;
        this.uitvindingDatum = LocalDate.of(LocalDate.now().getYear(),LocalDate.now().getMonth(),LocalDate.now().getDayOfMonth()-1);
        this.inhoud = 0.0;
        this.verpaktPer = 0;
        this.barcode = "012345678910";
        aantal++;
    }
    public Drank(String naam, String herkomst, Categorie categorie, LocalDate uitvindingDatum, double inhoud, int verpaktPer, String barcode) {
        this.setNaam(naam);
        this.setHerkomst(herkomst);
        this.setCategorie(categorie);
        this.setUitvindingDatum(uitvindingDatum);
        this.setInhoud(inhoud);
        this.setVerpaktPer(verpaktPer);
        this.setBarcode(barcode);
        aantal++;
    }
    @CanRun
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    @CanRun
    public void setInhoud(double inhoud) {
        try {
            if (inhoud<=0) {
                throw new IllegalArgumentException();
            }
            this.inhoud = inhoud;
        } catch(IllegalArgumentException e) {
            System.out.println("Inhoud kan niet 0 of onder de 0 liggen.");
        }
    }

    @CanRun
    public void setVerpaktPer(int verpaktPer) {
        try {
            if (verpaktPer<=0) {
                throw new IllegalArgumentException();
            }
            this.inhoud = inhoud;
        } catch(IllegalArgumentException e) {
            System.out.println("Verpakt per kan niet 0 of onder de 0 liggen.");
        }
    }

    @CanRun("123388484883")
    public void setBarcode(String barcode) {
        try {
            if (barcode.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.barcode = barcode;
        } catch(IllegalArgumentException e) {
            System.out.println("Barcode kan niet leeg zijn!");
        }
    }

    public String getBarcode() {
        return barcode;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public double getInhoud() {
        return inhoud;
    }

    public int getVerpaktPer() {
        return verpaktPer;
    }

    public static int getAantal() {
        return aantal;
    }

    @Override
    public int hashCode() {
        return this.barcode.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Drank object = (Drank) obj;
        if (object.barcode.equals(this.barcode)) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Drank drank) {
        return this.getBarcode().compareTo(drank.getBarcode());
    }

    @Override
    public String toString() {
        return String.format("%-15s (%s) \t %-11s  %s",this.naam,this.uitvindingDatum.getYear(),this.herkomst,this.categorie.toString());
    }

}
