package be.kdg.drankproject;

import be.kdg.drankproject.model.Drank;
import be.kdg.drankproject.model.Dranken;
import be.kdg.drankproject.model.Vloeistof;
import be.kdg.drankproject.reflection.ReflectionTools;

import java.sql.Ref;

public class Demo_3 {


    public static void main(String[] args) {
        ReflectionTools.classAnalysis(Vloeistof.class,Drank.class,Dranken.class);
        System.out.println();
        System.out.printf("Aangemaakt object door runAnnotated: \n");
        System.out.printf("%s",ReflectionTools.runAnnotated(Drank.class).toString());
    }
}
