package be.kdg.drankproject.reflection;

import java.lang.reflect.*;

public class ReflectionTools {

    @SafeVarargs
    public static void classAnalysis(Class ...a) {
        for (Class aClass: a) {
            System.out.printf("Analyse van de klasse: %s ===============================================\n",aClass.getSimpleName());
            System.out.printf("Fully qualified name : %s\n", aClass.getName());
            System.out.printf("Naam van de superklasse: %s\n",aClass.getSuperclass().getSimpleName());
            System.out.printf("Naam van de package : package %s\n",aClass.getPackage().getName());
            for (Class interfac: aClass.getInterfaces()) {
                System.out.printf("Interfaces: %s\n", interfac.getSimpleName());
            }
            System.out.println("Constructors:");
            for (Constructor constructor: aClass.getConstructors()) {
                System.out.printf("\t%s\n",constructor.getName());
            }

            System.out.print("attributen\t:");
            for (Field field : aClass.getDeclaredFields()) {
                System.out.printf(" %s",field.getName());
            }

            System.out.print("\ngetters\t:");
            for (Method method: aClass.getMethods()) {
                if (method.getName().contains("get")) {
                    System.out.printf(" %s",method.getName());
                }
            }
            System.out.print("\nsetters\t:");
            for (Method method: aClass.getMethods()) {
                if (method.getName().contains("set")) {
                    System.out.printf(" %s",method.getName());
                }
            }

            System.out.print("\nandere methoden:\t:");
            for (Method method: aClass.getMethods()) {
                if (!method.getName().contains("get") && !method.getName().contains("set")) {
                    System.out.printf(" %s",method.getName());
                }
            }
            System.out.println();
        }

    }




    public static Object runAnnotated (Class aClass) {
        Object o;
        try {
             o = aClass.getDeclaredConstructor().newInstance();
            for (Method method : aClass.getMethods()) {
                CanRun canRun = method.getAnnotation(CanRun.class);
                if (canRun!=null) {
                    if (method.getParameterCount() == 1) {
                        if ( method.getGenericParameterTypes()[0] == String.class) {
                            method.invoke(o,canRun.value());
                        }
                    }
                }
            }
            return o;

        } catch (Exception e){
                e.printStackTrace();
        }
        return null;
    }

}
