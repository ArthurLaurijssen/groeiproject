package be.kdg.drankproject.model;

import java.time.LocalDate;
/**
 * Maakt een nieuw Drank object.
 *
 * @author Arthur Laurijssen
 * @version 1.1
 * @see <a>https://nl.wikipedia.org/wiki/Drank</>
 */
public class Drank implements Comparable<Drank>{

    /**
     * Geeft de naam v/d drank
     */
    private String naam;

    /**
     * Geeft de herkomst v/d drank
     */
    private String herkomst;

    /**
     * Geeft de barcode v/d drank
     */
    private String barcode;

    /**
     * Geeft de categorie waartoe de drank behoort
     * Voor meer informatie:
     *
     * @see Categorie
     */
    private Categorie categorie;
    /**
     * Geeft de datum waarop de drank is uitgevonden
     */
    private LocalDate uitvindingDatum;
    /**
     * Geeft de inhoud v/d fles v/d drank
     */
    private double inhoud;
    /**
     * Geeft per hoeveel stuks de drank verpakt is
     */
    private int verpaktPer;
    /**
     * Geeft het aantal drank objecten weer die al gecreerd zijn
     */
    private static int aantal = 0;
    /**
     * Maakt een nieuw object Drank aan met de default waarde.
     */
    public Drank() {
        setNaam("ongekent");
        setHerkomst("anoniem");
        setCategorie(Categorie.ONGEKENT);
        setUitvindingDatum(LocalDate.of(LocalDate.now().getYear(),LocalDate.now().getMonth(),LocalDate.now().getDayOfMonth()-1));
        setInhoud(1.0);
        setVerpaktPer(1);
        setBarcode("012345678910");
        aantal++;
    }
    /**
     * Maakt een nieuw object Drank aan.
     *
     * @param naam de naam van de drank
     * @param herkomst de herkomst van de drank
     * @param categorie de categorie waartoe de drank behoort
     * @param uitvindingDatum de datum wanneer de drank is uitgevonden
     * @param inhoud de inhoud van de drank in ml
     * @param verpaktPer Per hoeveel de drank verpakt is
     * @param barcode De barcode van de drank
     */
    public Drank(String naam, String herkomst, Categorie categorie, LocalDate uitvindingDatum, double inhoud, int verpaktPer,String barcode) {
        setNaam(naam);
        setHerkomst(herkomst);
        setCategorie(categorie);
        setUitvindingDatum(uitvindingDatum);
        setInhoud(inhoud);
        setVerpaktPer(verpaktPer);
        setBarcode(barcode);
        aantal++;
    }

    public void setNaam(String naam) {
        try {
            if (naam.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.naam = naam;
        } catch(IllegalArgumentException e) {
            System.out.println("Naam mag niet leeg zijn");
        }
    }

    public void setHerkomst(String herkomst) {
        try {
            if (herkomst.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.herkomst = herkomst;
        } catch(IllegalArgumentException e) {
            System.out.println("Herkomst kan niet leeg zijn");
        }
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public void setUitvindingDatum(LocalDate uitvindingDatum) {
        try {
            if (!uitvindingDatum.isBefore(LocalDate.now()))  {
                throw new IllegalArgumentException();
            }
            this.uitvindingDatum= uitvindingDatum;
        } catch(IllegalArgumentException e) {
            System.out.println("Kan niet in de toekomst zijn");
        }
    }

    public void setInhoud(double inhoud) {
        try {
            if (inhoud<=0) {
                throw new IllegalArgumentException();
            }
            this.inhoud = inhoud;
        } catch(IllegalArgumentException e) {
            System.out.println("Inhoud kan niet 0 of onder de 0 liggen.");
        }
    }

    public void setVerpaktPer(int verpaktPer) {
        try {
            if (verpaktPer<=0) {
                throw new IllegalArgumentException();
            }
            this.inhoud = inhoud;
        } catch(IllegalArgumentException e) {
            System.out.println("Verpakt per kan niet 0 of onder de 0 liggen.");
        }
    }

    public void setBarcode(String barcode) {
        try {
            if (barcode.isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.barcode = barcode;
        } catch(IllegalArgumentException e) {
            System.out.println("Barcode kan niet leeg zijn!");
        }
    }

    public String getNaam() {
        return naam;
    }

    public String getHerkomst() {
        return herkomst;
    }

    public String getBarcode() {
        return barcode;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public LocalDate getUitvindingDatum() {
        return uitvindingDatum;
    }

    public double getInhoud() {
        return inhoud;
    }

    public int getVerpaktPer() {
        return verpaktPer;
    }

    public static int getAantal() {
        return aantal;
    }

    /**
     * Creeerd een hashcode op basis v/d barcode
     *
     * @return hashcode van de barcode
     */
    @Override
    public int hashCode() {
        return this.barcode.hashCode();
    }

    /**
     * Controleerd of 2 dranken hetzelfde zijn
     * op basis v/d barcode
     *
     * @param obj van het type Drank
     * @return true als het dezelfde drank is en anders false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Drank object = (Drank) obj;
        if (object.barcode.equals(this.barcode)) {
            return true;

        }
        return false;
    }

    /**
     * Kijkt welke het hoogste is. Op basis van Barcode.
     * Word gebruikt voor het sorteren van dranken
     *
     * @return -1 als het kleiner is 0 als ze gelijk zijn en anders 1 als de drank groter is
     * @param drank drank object to compare to
     */
    @Override
    public int compareTo(Drank drank) {
        return this.getBarcode().compareTo(drank.getBarcode());
    }

    /**
     * Zet het drankObject om naar een String en returnt deze string
     *
     * @return Het aangemaakte String object van de Drank
     */
    @Override
    public String toString() {
        return String.format("%-15s (%s) \t %-11s  %s",this.naam,this.uitvindingDatum.getYear(),this.herkomst,this.categorie.toString());
    }

}
