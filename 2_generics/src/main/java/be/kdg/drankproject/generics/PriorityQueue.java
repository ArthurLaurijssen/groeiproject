package be.kdg.drankproject.generics;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.TreeMap;

/**
 * Maakt een nieuw PrioriteitsQueue object.
 *
 * @author Arthur Laurijssen
 * @version 1.1
 * @see <a>https://nl.wikipedia.org/wiki/Drank</>
 */
public class PriorityQueue<T> implements FIFOQueue {
    /**
     * Een map met alle objecten gesorteerd volgens Prioriteit en dan volgens toevoegingsdatum
     */
    private TreeMap<Integer, LinkedList<T>> map;

    /**
     * De constructor maakt een een prioriteit queueu aan.
     */
    public PriorityQueue() {
        this.map = new TreeMap<>(Comparator.reverseOrder());
    }

    /**
     * Het element en de prioriteit komen als parameters binnen. Onderzoek eerst of het element
     * nog niet voorkomt in de map (ongeacht de prioriteit). Indien niet, dan voeg je het achteraan toe in de
     * juiste prioriteitsreeks.
     *
     * @param element het toe te voegen element
     * @param priority de prioriteit van het toe te voegen element
     * @return false als het er al instaat anders als het gelukt is true
     */
    @Override
    public boolean enqueue(Object element, int priority) {
        for (LinkedList<T>  list:this.map.values()) {
            for (T elementInList : list) {
                if (elementInList.equals(element)) {
                    return false;
                }
            }
        }
        LinkedList<T> templist =  this.map.get(priority);
        if (templist == null) {
            templist = new LinkedList<T>();
        }
        templist.add((T)element);
        this.map.put(priority,templist);
        return true;
    }

    /**
     * Retourneer het element dat zich vooraan bevindt in de hoogste prioriteitsreeks.
     *
     * @return het object met de hoogste prioriteit dat het er het langst instaat
     */
    @Override
    public Object dequeue() {
        for (int i = 5;i>=1;i--) {
            if (this.map.get(i) != null && this.map.get(i).size()>=1) {
                return this.map.get(i).removeFirst();
            }
        }
        return null;
    }

    /**
     * Zoek het element op in de map. Indien gevonden, dan retourneer je de positie in de queue.
     * Indien niet gevonden: -1
     *
     * @param element het te zoeken element
     * @return de locatie waar het element staat. Indien niet gevonden -1
     */
    @Override
    public int search(Object element) {
        int positie = 0;
        for (int i = 5;i>=1;i--) {
            LinkedList<T> list = this.map.get(i);
            if (list!=null) {
                for (T elementInList : list) {
                    positie++;
                    if (elementInList.equals(element)) {
                        return positie;
                    }
                }
            }
        }
        return -1;
    }

    /**
     *
     * @return de groote van de queue
     */
    @Override
    public int getSize() {
        int positie=0;
        for (LinkedList<T>  list:this.map.values()) {
            for (T elementInList : list) {
                positie++;
            }
        }
        return positie;
    }

    /**
     * Zet de queue om naar een String en returnt deze string
     *
     * @return Het aangemaakte String object van de queue
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 5;i>=1;i--) {
            LinkedList<T> list = this.map.get(i);
            if (list != null) {
                for (T elementInList : list) {
                    stringBuilder.append(i);
                    stringBuilder.append( " : ");
                    stringBuilder.append(elementInList.toString());
                    stringBuilder.append("\n");
                }
            }

        }
        return stringBuilder.toString();
    }
}
