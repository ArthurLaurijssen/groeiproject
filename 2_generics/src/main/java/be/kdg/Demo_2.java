package be.kdg;

import be.kdg.drankproject.data.Data;
import be.kdg.drankproject.generics.PriorityQueue;
import be.kdg.drankproject.model.Drank;

import java.util.List;

public class Demo_2 {
    public static void main(String[] args) {
        /*
        PriorityQueue<String> myQueue = new PriorityQueue<>();
        myQueue.enqueue("alfa", 2);
        myQueue.enqueue("beta", 5);
        myQueue.enqueue("gamma", 2);
        myQueue.enqueue("delta", 3);
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: " + myQueue.getSize());
        System.out.println("positie van gamma: " + myQueue.search("gamma"));
        for(int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue.dequeue());
        }
        System.out.println("Size na dequeue: " + myQueue.getSize());

        PriorityQueue<Drank> myDrankQue = new PriorityQueue<>();
        List<Drank> drankenList = Data.getData();
        myDrankQue.enqueue(drankenList.get(0),2);
        myDrankQue.enqueue(drankenList.get(4), 5);
        myDrankQue.enqueue(drankenList.get(3), 2);
        myDrankQue.enqueue(drankenList.get(8), 3);

        System.out.println("Overzicht van de Dranken PriorityQueue:");
        System.out.println(myDrankQue.toString());
        System.out.println("aantal: " + myDrankQue.getSize());
        System.out.println("positie van Coca Cola: " + myDrankQue.search(drankenList.get(0)));
        System.out.println("Dequeue: " + myDrankQue.dequeue());
        System.out.println("Size na dequeue: " + myDrankQue.getSize());*/

        var myQueue = new PriorityQueue<>();
        myQueue.enqueue("alfa", 2);
        myQueue.enqueue("beta", 5);
        myQueue.enqueue("gamma", 2);
        myQueue.enqueue("delta", 3);
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: " + myQueue.getSize());
        System.out.println("positie van gamma: " + myQueue.search("gamma"));
        for(int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue.dequeue());
        }
        System.out.println("Size na dequeue: " + myQueue.getSize());

        var myDrankQue = new PriorityQueue<>();
        var drankenList = Data.getData();
        myDrankQue.enqueue(drankenList.get(0),2);
        myDrankQue.enqueue(drankenList.get(4), 5);
        myDrankQue.enqueue(drankenList.get(3), 2);
        myDrankQue.enqueue(drankenList.get(8), 3);

        System.out.println("Overzicht van de Dranken PriorityQueue:");
        System.out.println(myDrankQue.toString());
        System.out.println("aantal: " + myDrankQue.getSize());
        System.out.println("positie van Coca Cola: " + myDrankQue.search(drankenList.get(0)));
        System.out.println("Dequeue: " + myDrankQue.dequeue());
        System.out.println("Size na dequeue: " + myDrankQue.getSize());


    }
}
