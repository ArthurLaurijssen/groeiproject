package be.kdg.java2.sms.service;


import be.kdg.java2.sms.database.StudentsDAO;
import be.kdg.java2.sms.service.implementation.StudentServicempl;
import be.kdg.java2.sms.service.models.Student;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
public class StudentServiceTest {
    private StudentService studentService;
    class DummyUserDAO implements StudentsDAO {

        @Override
        public void addStudent(Student student) {
            //nothing
        }

        @Override
        public List<Student> retrieveAll() {
            List<Student> students = new ArrayList<>();
            students.add(new Student("Arthur", LocalDate.of(1987, 2, 25), 1.98));
            return students;
        }
    }

    @Before
    public void setUp() throws Exception {
        StudentsDAO studentsDAO = new DummyUserDAO();
        studentService = new StudentServicempl(studentsDAO);
    }

    @Test
    public void getStudents() {
        try {
            studentService.addStudent(new Student("Arthur", LocalDate.of(1987, 2, 25), 1.98));
        } catch (Exception e) {
            fail("Foutmelding");
        }
    }

    @Test
    public void addStudent() {
        try {
            studentService.addStudent(new Student("Arthur", LocalDate.of(1987, 2, 25), 1.98));
        } catch (Exception e) {
            fail("Foutmelding");
        }
    }
}
