package be.kdg.java2.sms.service;


import be.kdg.java2.sms.database.UserDAO;
import be.kdg.java2.sms.exceptions.UserException;
import be.kdg.java2.sms.service.implementation.UserServicempl;
import be.kdg.java2.sms.service.models.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserServiceTest {
    private UserService userService;
    class DummyUserDAO implements UserDAO {
        @Override
        public User getUserByName(String userName) {
            return new User(userName, "test123456");
        }
        @Override
        public boolean addUser(String naam,String wachtwoord) {
            return true;//do nothing
        }
    }

    @Before
    public void setUp() throws Exception {
        userService = new UserServicempl(new DummyUserDAO());
    }

    @Test
    public void testAddUser() {
        try {
            userService.addUser("test2","12");
            fail("Zou een UserException moeten geven");
        } catch (UserException e) {

        }
    }
    @Test
    public void testLogin() {
        assertFalse("Zou false terug geven als user niet bestaat",userService.login("kajejejj","1234567891"));
        assertFalse("Zou false terug geven als paswoord verkeerd is",userService.login("test","1234567891"));
        assertTrue("Zou true geven als user bestaat en password juist is",userService.login("test","test123456"));
        assertTrue("Zou true geven als user bestaat en password juist is",userService.login("tesT","test123456"));
    }
}
