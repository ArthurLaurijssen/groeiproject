package be.kdg.java2.sms.database;

import be.kdg.java2.sms.service.models.Student;

import java.util.List;

public interface StudentsDAO {
    void addStudent(Student student);

    List<Student> retrieveAll();
}
