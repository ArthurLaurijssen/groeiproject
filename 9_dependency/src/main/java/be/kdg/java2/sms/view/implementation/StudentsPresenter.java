package be.kdg.java2.sms.view.implementation;

import be.kdg.java2.sms.service.models.Student;
import be.kdg.java2.sms.service.StudentService;
import be.kdg.java2.sms.view.Presenter;
import javafx.collections.FXCollections;

import java.util.List;
import java.util.logging.Logger;

public class StudentsPresenter implements Presenter {
    private StudentsViewmpl studentsView;
    private StudentService studentService;
    private static final Logger L = Logger.getLogger(StudentsPresenter.class.getName());

    public StudentsPresenter(StudentsViewmpl studentsView, StudentService studentService) {
        this.studentsView = studentsView;
        this.studentService = studentService;
        loadStudents();
        addEventHandlers();
    }

    private void addEventHandlers() {
        studentsView.getBtnSave().setOnAction(event -> {
            Student student = new Student(this.studentsView.getTfName().getText(),this.studentsView.getDpBirthday().getValue(),Double.valueOf(this.studentsView.getTfLength().getText()));

            if ( this.studentService.addStudent(student)) {
                L.info("Succesfully added student");
                loadStudents();
            } else {
                L.warning("Problem adding student");
            }

        });

    }
    private void loadStudents() {
        List<Student> myList = studentService.getAllStudents();
        studentsView.getTableView().setItems(FXCollections.observableList(myList));
    }
}
