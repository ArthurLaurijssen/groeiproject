package be.kdg.java2.sms.view.implementation;

import be.kdg.java2.sms.database.StudentsDAO;
import be.kdg.java2.sms.database.hsql.HSQLStudentsDAO;
import be.kdg.java2.sms.service.StudentService;
import be.kdg.java2.sms.service.UserService;
import be.kdg.java2.sms.service.implementation.StudentServicempl;
import be.kdg.java2.sms.view.Presenter;
import javafx.scene.Scene;

import java.util.logging.Logger;

public class LoginPresenter implements Presenter {
    private LoginViewmpl loginView;
    private UserService userService;
    private static final Logger L = Logger.getLogger(LoginPresenter.class.getName());

    public LoginPresenter(LoginViewmpl loginView, UserService userService) {
        this.loginView = loginView;
        this.userService = userService;
        addEventHandlers();
    }
   private void addEventHandlers() {
        this.loginView.getAddButton().setOnAction(event -> {
            LoginPresenter.L.info("add student");
            String username = loginView.getLoginNametextField().getText();
            String password = loginView.getPasswordField().getText();

            if (userService.addUser(username,password)) {
                LoginPresenter.L.info("User added");
            } else {
                LoginPresenter.L.warning("User added failed");
            }
        });
        this.loginView.getLoginButton().setOnAction(event -> {
            LoginPresenter.L.info("login student");
            String username = loginView.getLoginNametextField().getText();
            String password = loginView.getPasswordField().getText();
            if (userService.login(username,password)) {
                LoginPresenter.L.info("User login succesfull");
                StudentsViewmpl studentsView = new StudentsViewmpl();
                StudentsDAO studentsDAO =  new HSQLStudentsDAO();
                StudentServicempl studentsService = new StudentServicempl(studentsDAO);
                StudentsPresenter studentsPresenter = new StudentsPresenter(studentsView, studentsService);
                Scene scene = loginView.getScene();
                scene.setRoot(studentsView);
                scene.getWindow().sizeToScene();
            } else {
                LoginPresenter.L.warning("User login failed");
            }
        });
    }

}
