package be.kdg.java2.sms.view.implementation;

import be.kdg.java2.sms.service.models.Student;
import be.kdg.java2.sms.view.StudentsView;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.time.LocalDate;

public class StudentsViewmpl extends BorderPane implements StudentsView {
    private TableView tableView;
    private HBox hBox;
    private TextField tfName,tfLength;
    private DatePicker dpBirthday;
    private Button btnSave;

    public StudentsViewmpl() {
        initialiseNodes();
        layoutNodes();

    }
    private void initialiseNodes() {
        this.tableView = new TableView();
        this.btnSave = new Button("Save");
        this.dpBirthday = new DatePicker();
        this.tfLength = new TextField();
        this.tfName = new TextField();
        this.hBox = new HBox(this.tfName,this.dpBirthday,this.tfLength,this.btnSave);
    }

    private void layoutNodes() {
        this.dpBirthday.setPromptText("Birthday");
        this.tfLength.setPromptText("Length");
        this.tfName.setPromptText("Name");
        this.setCenter(this.tableView);
        this.hBox.setSpacing(10);
        this.setBottom(this.hBox);
        this.addTableView();
    }

    @Override
    public TableView getTableView() {
        return tableView;
    }

    private void addTableView() {
        TableColumn<String, Student> column1 = new TableColumn<>("Name");
        column1.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn<LocalDate, Student> column2 = new TableColumn<>("Birthday");
        column2.setCellValueFactory(new PropertyValueFactory<>("birthday"));
        TableColumn<String, Student> column3 = new TableColumn<>("Length");
        column3.setCellValueFactory(new PropertyValueFactory<>("length"));
        this.tableView.getColumns().addAll(column1, column2, column3);
    }

    @Override
    public TextField getTfName() {
        return tfName;
    }

    @Override
    public TextField getTfLength() {
        return tfLength;
    }


    @Override
    public DatePicker getDpBirthday() {
        return dpBirthday;
    }

    @Override
    public Button getBtnSave() {
        return btnSave;
    }
}
