package be.kdg.java2.sms.service;

import be.kdg.java2.sms.exceptions.UserException;

public interface UserService {
    boolean addUser(String naam, String wachtwoord) throws UserException;

    boolean login(String naam, String wachtwoord);
}
