package be.kdg.java2.sms.database.hsql;

import be.kdg.java2.sms.database.DataSource;
import be.kdg.java2.sms.database.StudentsDAO;
import be.kdg.java2.sms.exceptions.StudentException;
import be.kdg.java2.sms.service.models.Student;
import be.kdg.java2.sms.exceptions.UserException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class HSQLStudentsDAO implements StudentsDAO {
    private static final Logger L = Logger.getLogger(HSQLStudentsDAO.class.getName());
    private Connection connection;

    public HSQLStudentsDAO() {
        try {
            this.connection= DataSource.getInstance().getConnection();
        } catch (SQLException e) {
            L.info("Problems getting connection");
            throw new UserException(e);
        }
    }
    @Override
    public void addStudent(Student student) {
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement("INSERT INTO STUDENTS VALUES (NULL,?,?,?)");
            preparedStatement.setString(1,student.getName());
            preparedStatement.setDate(2,Date.valueOf(student.getBirthday()));
            preparedStatement.setDouble(3,student.getLength());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e){
            L.warning("SQL Exception while adding student");
            throw new StudentException(e.getMessage());

        }
    }
    @Override
    public List<Student> retrieveAll() {
        List<Student> studentsList = new ArrayList<>();
        try {
            ResultSet rs = this.connection.createStatement().executeQuery("SELECT * FROM STUDENTS");
            while (rs.next()) {
                String name = rs.getString(2);
                LocalDate birth =LocalDate.parse(rs.getString(3));
                double length = rs.getDouble(4);
                studentsList.add(new Student(name,birth,length));
            }
        } catch (SQLException e){
            L.warning("SQL Exception while retrieving students");
            throw new StudentException(e.getMessage());

        }
        return studentsList;
    }
}
