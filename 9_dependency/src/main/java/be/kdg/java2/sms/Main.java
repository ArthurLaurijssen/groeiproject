package be.kdg.java2.sms;

import be.kdg.java2.sms.database.hsql.HSQLUserDAO;
import be.kdg.java2.sms.database.UserDAO;
import be.kdg.java2.sms.service.UserService;
import be.kdg.java2.sms.service.implementation.UserServicempl;
import be.kdg.java2.sms.view.implementation.LoginPresenter;
import be.kdg.java2.sms.view.implementation.LoginViewmpl;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.logging.Logger;

public class Main extends Application {

    private static final Logger L = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
            L.info("Starting Student Management System");
            launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {




        LoginViewmpl view = new LoginViewmpl();
        UserDAO userDAO = new HSQLUserDAO();
        UserService userService = new UserServicempl(userDAO);
        LoginPresenter loginPresenter = new LoginPresenter(view,userService);

        Scene scene = new Scene(view);
        primaryStage.setScene(scene);
        primaryStage.show();



    }
}
