package be.kdg.java2.sms.database.hsql;

import be.kdg.java2.sms.database.DataSource;
import be.kdg.java2.sms.database.UserDAO;
import be.kdg.java2.sms.service.models.User;
import be.kdg.java2.sms.exceptions.UserException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

public class HSQLUserDAO implements UserDAO {
    private static final Logger L = Logger.getLogger(HSQLUserDAO.class.getName());

    private Connection connection;

    public HSQLUserDAO()  {
        try {
            connection= DataSource.getInstance().getConnection();
        } catch (SQLException e) {
            L.info("Problems getting connection");
            throw new UserException(e);
        }

    }

    @Override
    public boolean addUser(String naam, String wachtwoord) {
        try {
            if (getUserByName(naam) == null) {
                PreparedStatement prep = connection.prepareStatement("INSERT INTO USERS VALUES (NULL,?,?)");
                prep.setString(1,naam);
                prep.setString(2,wachtwoord);
                prep.executeUpdate();
                prep.close();

            }
        } catch (SQLException e) {
            L.warning("Error can't execute adduser");
            throw new UserException(e);
        }
        return false;
    }
    @Override
    public User getUserByName(String naam) {
        try {
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM USERS WHERE NAME=?");
            prep.setString(1,naam);
            ResultSet rs = prep.executeQuery();
            if(rs.next()){
                return new User(rs.getString("NAME"), rs.getString("PASSWORD"));
            }
            prep.close();
        } catch (SQLException e) {
            L.warning("Error can't execute getUserByName");
            throw new UserException(e);
        }
        return null;
    }
}
