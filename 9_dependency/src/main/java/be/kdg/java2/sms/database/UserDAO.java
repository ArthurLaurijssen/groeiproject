package be.kdg.java2.sms.database;

import be.kdg.java2.sms.service.models.User;

public interface UserDAO {
    boolean addUser(String naam, String wachtwoord);

    User getUserByName(String naam);
}
