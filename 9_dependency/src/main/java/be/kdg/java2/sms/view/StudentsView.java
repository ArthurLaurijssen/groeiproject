package be.kdg.java2.sms.view;

import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public interface StudentsView {
    TableView getTableView();

    TextField getTfName();

    TextField getTfLength();

    DatePicker getDpBirthday();

    Button getBtnSave();
}
