package be.kdg.java2.sms.view.implementation;

import be.kdg.java2.sms.view.LoginView;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.util.logging.Logger;

public class LoginViewmpl extends BorderPane implements LoginView {

    private TextField loginNametextField;
    private PasswordField passwordField;
    private Label nameLabel,passwordLabel;
    private HBox hBox;
    private Button loginButton,addButton;

    private GridPane gridPane;


    private static final Logger L = Logger.getLogger(LoginViewmpl.class.getName());


    public LoginViewmpl() {
        L.info("Constructing LoginView!");
        this.initialiseNodes();
        this.layoutNodes();
    }


    private void initialiseNodes() {
        this.loginNametextField = new TextField();
        this.passwordField = new PasswordField();
        this.addButton = new Button("Add");
        this.loginButton = new Button("Login");
        this.gridPane = new GridPane();
        this.nameLabel = new Label("User: ");
        this.passwordLabel = new Label("Password: ");
        this.hBox = new HBox(this.loginButton,this.addButton);

    }
    private void layoutNodes() {
        this.gridPane.add(this.nameLabel,0,0);
        this.gridPane.add(this.loginNametextField,1,0);
        this.gridPane.add(this.passwordLabel,0,1);
        this.gridPane.add(this.passwordField,1,1);
        this. gridPane.setGridLinesVisible(true);
        this.gridPane.setHgap(6);
        this.gridPane.setVgap(6);
        BorderPane.setMargin(gridPane,new Insets(5));
        this.setCenter(gridPane);
        this.hBox.setSpacing(10);
        this.setBottom(hBox);
        BorderPane.setMargin(hBox,new Insets(10));
    }


    @Override
    public Button getLoginButton() {
        return loginButton;
    }

    @Override
    public Button getAddButton() {
        return addButton;
    }

    void setLoginButton(Button loginButton) {
        this.loginButton = loginButton;
    }

    void setAddButton(Button addButton) {
        this.addButton = addButton;
    }

    @Override
    public TextField getLoginNametextField() {
        return loginNametextField;
    }

    @Override
    public PasswordField getPasswordField() {
        return passwordField;
    }

    void setLoginNametextField(TextField loginNametextField) {
        this.loginNametextField = loginNametextField;
    }

    void setPasswordField(PasswordField passwordField) {
        this.passwordField = passwordField;
    }
}
