package be.kdg.java2.sms.service.implementation;

import be.kdg.java2.sms.database.UserDAO;
import be.kdg.java2.sms.exceptions.UserException;
import be.kdg.java2.sms.service.UserService;
import be.kdg.java2.sms.service.models.User;

import java.util.logging.Logger;

public class UserServicempl implements UserService {
    private UserDAO userDAO;

    private static final Logger L = Logger.getLogger(UserServicempl.class.getName());

    public UserServicempl(UserDAO userDAO)
    {
        this.userDAO = userDAO;
    }

    @Override
    public boolean addUser(String naam, String wachtwoord) throws UserException {
        L.info(String.format("Creating user with credentials: %s , %s",naam,wachtwoord));
        if (wachtwoord.length() < 10) {
            throw new UserException("Wachtwoord te kort moet minimaal 10 lang zijn");
        }
        //check of user al bestaad:
        if (userDAO.getUserByName(naam) != null) {
            throw new UserException("Gebruiker bestaad al");
        }
        userDAO.addUser(naam,wachtwoord);
        return true;
    }
    @Override
    public boolean login(String naam, String wachtwoord) {
        L.info(String.format("Trying to login with: %s , %s",naam,wachtwoord));
        User user = userDAO.getUserByName(naam.toLowerCase());
        if (user != null && user.getPassword().equals(wachtwoord)) {
            return true;
        }
        return false;
    }
}
