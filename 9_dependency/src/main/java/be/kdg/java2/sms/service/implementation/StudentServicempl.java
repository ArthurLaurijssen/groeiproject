package be.kdg.java2.sms.service.implementation;

import be.kdg.java2.sms.database.StudentsDAO;
import be.kdg.java2.sms.service.StudentService;
import be.kdg.java2.sms.service.models.Student;

import java.util.List;

public class StudentServicempl implements StudentService {
    private StudentsDAO studentsDAO;

    public StudentServicempl(StudentsDAO studentsDAO) {
        this.studentsDAO = studentsDAO;
    }

    @Override
    public List<Student> getAllStudents() {
        return studentsDAO.retrieveAll();
    }
    @Override
    public boolean addStudent(Student student) {
        if (getAllStudents().contains(student)) {
            return false;
        }
        else {
            this.studentsDAO.addStudent(student);
            return true;
        }
    }
}
