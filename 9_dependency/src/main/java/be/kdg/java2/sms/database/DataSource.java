package be.kdg.java2.sms.database;

import java.sql.*;
import java.util.logging.Logger;

public class DataSource {
    private static DataSource onlyInstance;
    private Connection connection;
    private Logger L = Logger.getLogger(DataSource.class.getName());


    private DataSource() {
        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:database/studentdb","sa","");
            this.createTables();
        } catch (SQLException e) {
            e.printStackTrace();
            L.warning("Can't establish connection");
        }

    }


    public static DataSource getInstance() throws SQLException {
        if(onlyInstance==null){
            onlyInstance = new DataSource();}
        return onlyInstance;
    }


    private void createTables() throws SQLException {
        try {
            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "USERS", null);
            if (!tables.next()) {
                L.info("Creating table USERS...");
                Statement statement = connection.createStatement();
                statement.execute("CREATE TABLE USERS (ID IDENTITY NOT NULL , NAME VARCHAR(60) NOT NULL, PASSWORD VARCHAR(20) NOT NULL)");
                statement.close();
            }

            tables = dbm.getTables(null, null, "STUDENTS", null);
            if (!tables.next()) {
                L.info("Creating table STUDENTS");
                Statement statement = connection.createStatement();
                statement.execute("CREATE TABLE STUDENTS (ID IDENTITY NOT NULL , NAME VARCHAR(100) NOT NULL, BIRTHDAY DATE, LENGTH DOUBLE)");
                statement.close();
            }
        }catch (SQLException e){
            L.warning("Problem creating tables");
            throw e;
        }

    }

    public Connection getConnection() {
        return connection;

    }
    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            L.info("Can't close connection");
        }
    }
}
