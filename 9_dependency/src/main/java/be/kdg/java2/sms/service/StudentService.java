package be.kdg.java2.sms.service;

import be.kdg.java2.sms.service.models.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudents();

    boolean addStudent(Student student);
}
